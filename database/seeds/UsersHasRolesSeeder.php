<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;

class UsersHasRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::select('id', 'is_admin')->get();
        
        foreach ($users as $user) {
            switch ($user->is_admin) {
                case 1:
                    $user->assignRole('Account Admin', 'Event Admin', 'Instructor');
                    break;

                case 3:
                    $user->assignRole('Instructor');
                    break;

                case 4:
                    $user->assignRole('Event Admin');
                    break;

                case 5:
                    $user->assignRole('Account Admin');
                    break;

                default:
                    break;
            }
        }
    }
}
