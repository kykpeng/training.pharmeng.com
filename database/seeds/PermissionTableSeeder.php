<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'course-list',
            'course-create',
            'course-edit',
            'course-delete',
            'generate-settings',
            'payment-settings',
            'users-settings',
            'about-settings'
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // create roles and assign created permissions
        // this can be done as separate statements
        $role1 = Role::create(['name' => 'Instructor'])
        ->givePermissionTo([
            'course-list',
            'course-create',
            'course-edit',
            'course-delete',
            'generate-settings',
            'payment-settings',
            'users-settings',
            'about-settings'
        ]);

        // or may be done by chaining
        $role2 = Role::create(['name' => 'Event Admin'])
        ->givePermissionTo([
            'course-list',
            'course-create',
            'course-edit',
            'course-delete',
            'generate-settings',
            'payment-settings',
            'users-settings',
            'about-settings'
        ]);

        $role3 = Role::create(['name' => 'Account Admin']);
        $role3->givePermissionTo(Permission::all());

        // $user = User::find(1);
        // $user->assignRole($role3);

        // create demo users
        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Instructor User',
        //     'email' => 'instructor@example.com',
        // ]);

        // $user->assignRole($role1);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Event Admin User',
        //     'email' => 'eventadmin@example.com',
        // ]);
        // $user->assignRole($role2);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Account Admin User',
        //     'email' => 'admin@example.com',
        // ]);
        // $user->assignRole($role3);
    }
}
