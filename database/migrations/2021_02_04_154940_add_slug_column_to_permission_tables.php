<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugColumnToPermissionTables extends Migration
{
//     /**
//      * Run the migrations.
//      *
//      * @return void
//      */
//     public function up()
//     {
//         $tableNames = config('permission.table_names');

//         Schema::table($tableNames['permissions'], function (Blueprint $table) {
//             $table->string('slug')->after('name')->unique();
//         });

//         Schema::table($tableNames['roles'], function (Blueprint $table) {
//             $table->string('slug')->after('name')->unique();
//         });
//     }

//     /**
//      * Reverse the migrations.
//      *
//      * @return void
//      */
//     public function down()
//     {
//         $tableNames = config('permission.table_names');

//         Schema::table($tableNames['permissions'], function (Blueprint $table) {
//             $table->dropColumn('slug');
//         });

//         Schema::table($tableNames['roles'], function (Blueprint $table) {
//             $table->dropColumn('slug');
//         });
//     }
}