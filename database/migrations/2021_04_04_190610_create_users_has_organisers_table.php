<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersHasOrganisersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_has_organisers', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('organiser_id');

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('organiser_id')->references('id')->on('organisers')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['user_id','organiser_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_has_organisers');
    }
}
