<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTicketEventAccessCodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ticket_event_access_code', function(Blueprint $table)
		{
			$table->foreign('event_access_code_id')->references('id')->on('event_access_codes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('ticket_id')->references('id')->on('tickets')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ticket_event_access_code', function(Blueprint $table)
		{
			$table->dropForeign('ticket_event_access_code_event_access_code_id_foreign');
			$table->dropForeign('ticket_event_access_code_ticket_id_foreign');
		});
	}

}
