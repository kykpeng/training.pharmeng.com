<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventAccessCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_access_codes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_id')->unsigned()->index('event_access_codes_event_id_foreign');
			$table->string('code')->default('');
			$table->integer('usage_count')->unsigned()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_access_codes');
	}

}
