module.exports = function (grunt) {
    //Initializing the configuration object
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // Task configuration
        cssmin: {
			landing: {
                files: {
                    './public/assets/stylesheet/landing.css': [
                        './node_modules/bootstrap/dist/css/bootstrap.css',
                        './node_modules/bootstrap/dist/css/bootstrap-theme.css',
                        './resources/assets/stylesheet/hero-slider.css',
                        // './node_modules/owlcarousel/owl-carousel/owl.carousel.css',
                        './resources/assets/stylesheet/owl-carousel.css',
                        './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        './resources/assets/stylesheet/landing.css',
                    ]
                }
			},
			NUSAGE: {
                files: {
                    './public/assets/stylesheet/NUSAGE.css': [
                        './node_modules/bootstrap/dist/css/bootstrap.css',
                        './node_modules/bootstrap/dist/css/bootstrap-theme.css',
                        './resources/assets/stylesheet/hero-slider.css',
                        // './node_modules/owlcarousel/owl-carousel/owl.carousel.css',
                        './resources/assets/stylesheet/owl-carousel.css',
                        './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        './resources/assets/stylesheet/NUSAGE.css',
                    ]
                }
			}
		},
        less: {
            development: {
                options: {
                    compress: true,
                    javascriptEnabled: true,
                },
                files: {
                    "./public/assets/stylesheet/application.css": "./public/assets/stylesheet/application.less",
                    "./public/assets/stylesheet/frontend.css": "./public/assets/stylesheet/frontend.less",
                }
            },

        },
        concat: {
            options: {
                separator: ';',
                stripBanners: {
                    block: true,
                    line: true
                },
            },
            js_frontend: {
                src: [
                    './public/vendor/jquery/dist/jquery.min.js',
                    './public/vendor/bootstrap/dist/js/bootstrap.js',
                    './public/vendor/jquery-form/jquery.form.js',
                    './public/vendor/RRSSB/js/rrssb.js',
                    './public/vendor/humane-js/humane.js',
                    './public/vendor/jquery.payment/lib/jquery.payment.js',
                    './resources/assets/javascript/app-frontend.js'
                ],
                dest: './public/assets/javascript/frontend.js',
            },
            js_backend: {
                src: [
                    './public/vendor/modernizr/modernizr.js',
                    './public/vendor/html.sortable/dist/html.sortable.js',
                    './public/vendor/bootstrap/dist/js/bootstrap.js',
                    './public/vendor/jquery-form/jquery.form.js',
                    './public/vendor/humane-js/humane.js',
                    './public/vendor/RRSSB/js/rrssb.js',
                    './public/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js',
                    './public/vendor/datetimepicker/dist/DateTimePicker.js',
                    './public/vendor/jquery-minicolors/jquery.minicolors.min.js',
                    './public/vendor/quill/quill.js',
                    './resources/assets/javascript/app-backend.js'
                ],
                dest: './public/assets/javascript/backend.js',
            },
            js_landing: {
                src: [
                    './node_modules/moment/moment.js',
                    './node_modules/moment-timezone/builds/moment-timezone-with-data.js',
                    './resources/assets/javascript/app-landing.js',
                    './resources/assets/javascript/modernizr-2.8.3-respond-1.4.2.min.js',
                    './node_modules/@fortawesome/fontawesome-free/js/all.js'
                ],
                dest: './public/assets/javascript/landing.js',
            },
            js_landing_defer: {
                src: [
                    './node_modules/jquery/dist/jquery.js',
                    './node_modules/bootstrap/dist/js/bootstrap.js',
                    './node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
                    './resources/assets/javascript/hero-slider.js',
                    './node_modules/lightbox2/dist/js/lightbox.js',
                    './node_modules/mixitup/dist/mixitup.js',
                    './node_modules/owlcarousel/owl-carousel/owl.carousel.js',
                    './resources/assets/javascript/app-landing-defer.js'
                ],
                dest: './public/assets/javascript/landing-defer.js',
            },
            js_NUSAGE: {
                src: [
                    './node_modules/moment/moment.js',
                    './node_modules/moment-timezone/builds/moment-timezone-with-data.js',
                    './resources/assets/javascript/app-landing.js',
                    './resources/assets/javascript/modernizr-2.8.3-respond-1.4.2.min.js',
                    './node_modules/@fortawesome/fontawesome-free/js/all.js'
                ],
                dest: './public/assets/javascript/NUSAGE.js',
            },
            js_NUSAGE_defer: {
                src: [
                    './node_modules/jquery/dist/jquery.js',
                    './node_modules/bootstrap/dist/js/bootstrap.js',
                    './node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
                    './resources/assets/javascript/hero-slider.js',
                    './node_modules/lightbox2/dist/js/lightbox.js',
                    './node_modules/mixitup/dist/mixitup.js',
                    './node_modules/owlcarousel/owl-carousel/owl.carousel.js',
                    './resources/assets/javascript/app-landing-defer.js'
                ],
                dest: './public/assets/javascript/NUSAGE-defer.js',
            },
        },
        uglify: {
            options: {
                mangle: true,  // Use if you want the names of your functions and variables unchanged
                preserveComments: false,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %> */',

            },
            frontend: {
                files: {
                    './public/assets/javascript/frontend.js': ['<%= concat.js_frontend.dest %>'],
                }
            },
            backend: {
                files: {
                    './public/assets/javascript/backend.js': ['<%= concat.js_backend.dest %>'],
                }
            },
            landing: {
                files: {
                    './public/assets/javascript/landing.js': ['<%= concat.js_landing.dest %>'],
                }
            },
            landing_defer: {
                files: {
                    './public/assets/javascript/landing-defer.js': ['<%= concat.js_landing_defer.dest %>'],
                }
            },
            NUSAGE: {
                files: {
                    './public/assets/javascript/NUSAGE.js': ['<%= concat.js_NUSAGE.dest %>'],
                }
            },
            NUSAGE_defer: {
                files: {
                    './public/assets/javascript/NUSAGE-defer.js': ['<%= concat.js_NUSAGE_defer.dest %>'],
                }
            },
        },
        watch: {
            scripts: {
                files: ['./public/assets/**/*.js','./resources/assets/**/*.js'],
                tasks: ['default'],
                options: {
                    spawn: false,
                },
            },
        }
    });

    // Plugin loading
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Task definition
    grunt.registerTask('default', ['cssmin', 'less', 'concat']);
    grunt.registerTask('deploy', ['cssmin', 'less', 'concat', 'uglify']);
    grunt.registerTask('js', ['concat']);
    grunt.registerTask('styles', ['concat_css']);
    grunt.registerTask('minify', ['uglify']);
};
