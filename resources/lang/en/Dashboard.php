<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2018/04/19 18:57:21 
*************************************************************************/

return array (
    //============================== New strings to translate ==============================//
    'this_event_has_started' => 'This training has started.',
    //==================================== Translations ====================================//
    'create_tickets' => 'Create sign-ups',
    'edit_event_page_design' => 'Edit Course Page Design',
    'edit_organiser_fees' => 'Edit Organiser Fees',
    'event_page_visits' => 'Course Page Views',
    'event_url' => 'Course Sign-Up URL',
    'event_views' => 'Course Views',
    'generate_affiliate_link' => 'Generate Affiliate link',
    'orders' => 'Orders',
    'quick_links' => 'Quick Links',
    'registrations_by_ticket' => 'Registrations By Sign-Up',
    'sales_volume' => 'Sales Volume',
    'share_event' => 'Share Course',
    'this_event_is_on_now' => 'This course is on now',
    'ticket_sales_volume' => 'Sign-Up Sales Volume',
    'tickets_sold' => 'Sign-Ups Sold',
    'website_embed_code' => 'Website Embed Code',
);