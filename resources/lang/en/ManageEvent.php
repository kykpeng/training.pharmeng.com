<?php

return array (
  'all_attendees' => 'All Attendees',
  'all_attendees_cancelled' => 'All attendees in this order have been cancelled.',
  'all_order_refunded' => 'All :money of this order has been refunded.',
  'amount' => 'Amount',
  'attendee_cancelled' => 'Cancelled',
  'attendee_cancelled_help' => 'This attendee has been cancelled',
  'attendees_file_requirements' => 'File must be .csv and the first line must contain first_name,last_name,email',
  'attendize_qrcode_check_in' => 'Attendize QRCode Check-in',
  'cancel_attendee_title' => 'Cancel :cancel',
  'cancel_description' => 'Cancelling Attendees will remove them from the attendee list.',
  'cancel_notify' => 'Notify :name their registration has been cancelled.',
  'cancel_order_:ref' => 'Cancel Order: :ref',
  'cancel_refund' => 'If you would like to refund the order which this attendee belongs to you can do so <strong><a href=":url">here</a></strong>.',
  'cancel_refund_user' => 'Refund :name for their registration.',
  'cant_refund_here' => 'Sorry, you can\'t refund :gateway payments here. You will have to do it on their website.',
  'check-in' => 'Check-In',
  'checkin_search_placeholder' => 'Search by Attendee Name, Order Reference, Attendee Reference...',
  'close' => 'close',
  'confirm_cancel' => 'Confirm cancel attendee',
  'confirm_order_cancel' => 'Confirm Order Cancel',
  'create_attendees' => 'Create Attendees',
  'create_ticket' => 'Create Sign-Up',
	'default_currency' => 'Default currency',
  'download_pdf_ticket' => 'Download PDF Registration Ticket',
  'edit_attendee' => 'Edit Attendee',
  'edit_attendee_title' => 'Edit :attendee',
  'edit_order_title' => 'Order: :order_ref',
  'edit_question' => 'Edit Question',
  'edit_ticket' => 'Edit Registration',
  'end_sale_on' => 'End Sale On',
  'event_not_live' => 'This course is not visible to the public.',
  'event_page' => 'Course Page',
  'event_tools' => 'Course Tools',
  'export' => 'Export',
  'go_to_attendee_name' => 'Go to attendee :name',
  'hide_this_ticket' => 'Hide This Registration',
  'import_file' => 'Import File',
  'invite_attendee' => 'Invite Attendee',
  'invite_attendees' => 'Invite Attendees',
  'issue_full_refund' => 'Issue full refund',
  'issue_partial_refund' => 'Issue partial refund',
  'manage_order_title' => 'Order: :order_ref',
  'mark_payment_received' => 'Mark Payment Received',
  'maximum_tickets_per_order' => 'Maximum Registrations Per Order',
  'message_attendee_title' => 'Message :attendee',
  'message_attendees' => 'Message',
  'message_attendees_title' => 'Message Attendees',
  'message_order' => 'Message :order',
  'minimum_tickets_per_order' => 'Minimum Registrations Per Order',
  'more_options' => 'More options',
  'no_attendees_matching' => 'No attendees matching',
  'no_attendees_yet' => 'No Attendees yet',
  'no_attendees_yet_text' => 'Attendees will appear here once they successfully registered for your course, or, you can manually invite attendees yourself.',
  'no_orders_yet' => 'No orders yet',
  'no_orders_yet_text' => 'New orders will appear here as they are created.',
  'order_contact_will_receive_instructions' => 'The order contact will be instructed to send any reply to :email',
  'order_details' => 'Order Details',
  'order_overview' => 'Order Overview',
  'order_ref' => 'Order: #:order_ref',
  'order_refunded' => ':money of this order has been refunded.',
  'price_placeholder' => 'E.g: 299',
  'print_attendee_list' => 'Print Attendee List',
  'print_tickets' => 'Print Registrations',
  'publish_it' => 'Publish it',
  'qr_instructions' => 'Put the QR code in front of your Camera (Not too close)',
  'quantity_available' => 'Quantity Available',
  'quantity_available_placeholder' => 'E.g: 100 (Leave blank for unlimited)',
  'refund_amount' => 'Refund amount',
  'refund_this_order?' => 'Refund this order?',
  'resend_ticket' => 'Resend Registration',
  'resend_ticket_help' => 'The attendee will be sent another copy of their registration to :email',
  'resend_ticket_to_attendee' => 'Resend Registration to :attendee',
  'resend_tickets' => 'Resend Registrations',
  'result_for' => 'result(s) for',
  'save_question' => 'Save Question',
  'save_ticket' => 'Save registration',
  'scan_another_ticket' => 'Scan Another Registration',
  'select_all' => 'Select All',
  'select_attendee_to_cancel' => 'Select any attendee registrations you wish to cancel.',
  'send_invitation_n_ticket_to_attendees' => 'Send invitation & registration to attendees',
  'send_message' => 'Send Message',
  'send_ticket' => 'Send registration',
  'start_sale_on' => 'Start Sale On',
  'surveys' => 'Surveys',
  'this_order_is_awaiting_payment' => 'This order is awaiting payment.',
  'ticket' => 'Registration',
  'ticket_description' => 'Registration Description',
  'ticket_price' => 'Registration Fee',
  'ticket_title' => 'Sign-Up Title',
  'ticket_title_placeholder' => 'E.g: Individual Student',
  'update_order' => 'Update Order',
  'widgets' => 'Widgets',
  'LLH:obsolete' => 
  array (
    'create_question' => 'Create Question',
  ),
);