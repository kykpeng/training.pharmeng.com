<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2018/04/26 11:05:24 
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'attendize_register' => 'Thank you for registering for Training.PharmEng',
  'invite_user' => ':name added you to an :app account.',
  'message_regarding_event' => 'Message Regarding: :event',
  'organiser_copy' => '[Organiser Copy]',
  'refund_from_name' => 'You have received a refund from :name',
  'your_ticket_cancelled' => 'Your registration has been cancelled',
  'your_ticket_for_event' => 'Your registration for the course :event',
    //================================== Obsolete strings ==================================//
  'LLH:obsolete' => 
  array (

  ),
);
