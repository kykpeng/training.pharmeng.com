<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2018/04/26 10:54:45
*************************************************************************/

return array (
  'address_details' => 'Address Details',
  'address_line_1' => 'Address Line 1',
  'address_line_1_placeholder' => 'E.g: 45 Amoy St.',
  'address_line_2' => 'Address Line 2',
  'address_line_2_placeholder' => 'E.g: Raffles',
  'city' => 'City',
  'city_placeholder' => 'E.g: Singapore',
  'create_event' => 'Create New Course',
  'current_event_flyer' => 'Current Course Flyer',
  'customize_event' => 'Customize Course',
  'delete?' => 'Delete?',
  'enter_existing' => 'Select From Existing Venues',
  'enter_manual' => 'Enter Address Manually',
  'event_instructor' => 'Instructor:', // Modified by Kenny 2020-09-05
  'event_administrator' => 'Administrator:', // Modified by Kenny 2020-09-05
  'event_administrator_tooltip' => 'The Administrator sends email reminders and runs Zoom in the background', // Modified by Kenny 2020-09-05
  'event_description' => 'Course Description',
  // Modified by Kenny 2020-09-04
  'event_description_placeholder' => 'Required.  E.g: This course is designed to provide an overview of the various aspects of computer systems validation and related validation documents and to provide the basis for compliance and implementation. The course addresses the rules, tools, and techniques to develop and implement a validation program or to validate a specific system ...',
  'event_choose_address' => 'Choose where the training will be held',
  'event_choose_address_manual' => 'On-Site (Enter Manually)',
  'event_choose_address_automatic' => 'On-Site (Choose from Google Map)',
  'event_choose_address_zoom' => 'Zoom',
  // End Modification
  'event_end_date' => 'Course End Date',
  'event_flyer' => 'Course Flyer',
  'event_image' => 'Course Image (Flyer or Graphic etc.)',
  'event_image_position' => 'Course Image Position',
  'event_image_position_hide' => 'Hidden',
  'event_image_position_before' => 'Before',
  'event_image_position_after' => 'After',
  'event_image_position_left' => 'Left',
  'event_image_position_right' => 'Right',
  'event_orders' => 'Course Orders',
  'event_start_date' => 'Course Start Date',
  'event_title' => 'Course Title',
  'event_title_placeholder' => 'Required.  E.g: Computerized System Validation',
  // Modified by Kenny 2020-09-04
  'event_objective' => 'Course Objective',
  'event_objective_placeholder' => 'Optional.  E.g: To learn to establish validation programs; acquire understanding of the regulatory requirements ...',
  'event_outline' => 'Course Outline',
  'event_outline_placeholder' => 'Optional.  E.g:&#10;- Validation Overview&#10;- Regulations and Regulators&#10;GAMP (Good Automated Manufacturing Practices) ...',
  'event_outcome' => 'Learning Outcome',
  'event_outcome_placeholder' => 'Optional.  E.g: Upon completion of this course the attendees will be able to:&#10;- Understand the various regulations and guidelines from regulatory bodies and industrial bodies such as 21 CFR 11, GAMP, Annex 11 ...',
  'event_about_instructor' => 'About the Instructor',
  'event_about_instructor_placeholder' => 'Optional.  E.g: John Smith, B.Sc., is a Senior Consultant with 15 years of experience ...',
  // End Modification
  'event_visibility' => 'Course Visibility',
  'go_live' => 'Course Successfully Made Live! You can undo this action in course settings page.',
  'n_attendees_for_event' => ':num Attendee(s) for course: :name (:date)',
  'no_events_yet' => 'No Course Yet!',
  'no_events_yet_text' => 'Looks like you have yet to create a course. You can create one by clicking the button below.',
  'num_events' => ':num Courses',
  'or(manual/existing_venue)' => 'or',
  'payment_cancelled' => 'You cancelled your payment. You may try again.',
  'post_code' => 'Post Code',
  'post_code_placeholder' => 'E.g: 645688.',
  'print_attendees_title' => 'Attendees',
  'promote' => 'Promote',
  'promote_event' => 'Promote Course',
  'revenue' => 'Revenue',
  'save_changes' => 'Save Changes',
  'showing_num_of_orders' => 'Showing :0 orders out of :1 Total',
  'tickets_sold' => 'Registrations Sold',
  'venue_name' => 'Venue Name',
  'venue_name_placeholder' => 'E.g: Lecture Room, School of Pharmacy',
  'vis_hide' => 'Hide course from the public.',
  'vis_public' => 'Make course visible to the public.',
  'zoom_url' => 'Zoom Info', // Modified by Kenny 2020-09-05
  'zoom_url_tooltip' => 'NOTE: for Student and Instructor only; a PharmEng Administrator is required separately to Host the call', // Modified by Kenny 2020-09-05
);