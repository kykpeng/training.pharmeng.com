<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2018/04/19 17:07:35
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  'scan_another_ticket' => 'Scan Another Registration',
  'scanning' => 'Scanning',
  //==================================== Translations ====================================//
  'attendees' => 'Attendees',
  'check_in' => 'Check in: :event',
  'email' => 'Email',
  'email_address' => 'Email Address',
  'event_attendees' => 'Event Attendees',
  'first_name' => 'First Name',
  'last_name' => 'Last Name',
  'name' => 'Name',
  'ticket' => 'Registration',
  'reference' => 'Reference',
  'search_attendees' => 'Search Attendees...',
  'send_invitation_n_ticket_to_attendee' => 'Send invitation & registration to attendee.',
);
