<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2018/04/19 17:21:50 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  'event_page_preview' => 'Event Page Preview',
  //==================================== Translations ====================================//
  'course_flyer' => 'Course Flyer & Analytics', // Modified by Kenny 2020-09-05
  'background_options' => 'Background Options',
  'images_provided_by_pixabay' => 'Images Provided By <b>PixaBay.com</b>',
  // 'select_from_available_images' => 'Select From Available Images',
  'select_from_available_images' => 'Option 2: Picture Background', // Modified by Kenny 2020-09-05
  // 'use_a_colour_for_the_background' => 'Use a colour for the background',
  'use_a_colour_for_the_background' => 'Option 1: Solid Color Background', // Modified by Kenny 2020-09-05
);