<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2018/04/26 11:05:24 
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'apply' => 'Apply',
  'action' => 'Action',
  // 'affiliates' => 'Affiliates',
  'affiliates' => 'Step 5', // Modified by Kenny 2020-09-05
  // 'attendees' => 'Attendees',
  'attendees' => 'Reach Attendees', // Modified by Kenny 2020-09-05
  'back_to_login' => 'Back to login',
  'back_to_page' => 'Back To :page',
  'cancel' => 'Cancel',
  'delete' => 'Delete',
  'update' => 'Update',
  // 'customize' => 'Customize',
  'customize' => 'Edit Course', // Modified by Kenny 2020-09-05
  'dashboard' => 'Dashboard',
  'days' => 'days',
  'disable' => 'Disable',
  'disabled' => 'Disabled',
  'drag_to_reorder' => 'Drag to re-order',
  'edit' => 'Edit',
  'enable' => 'Enable',
  'enabled' => 'Enabled',
  'error_404' => 'Looks like the page you are looking for no longer exists or has moved.',
  'event_dashboard' => 'Course Dashboard',
  // 'event_menu' => 'Course Menu',
  // 'event_page_design' => 'Course Page Design',
  'event_menu' => ':name Menu',
  'event_page_design' => 'Step 2', // Modified by Kenny 2020-09-05
  'export' => 'Export',
  // 'general' => 'General',
  'general' => 'Step 1', // Modified by Kenny 2020-09-05
  'go_live' => 'Go Live', // Modified by Kenny 2020-09-05
  'hours' => 'hours',
  'main_menu' => 'Main Menu',
  'manage' => 'Manage',
  'message' => 'Message',
  'minutes' => 'minutes',
  'months_short' => '|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|',
  'no' => 'No',
  // 'order_form' => 'Order Form',
  'order_form' => 'Step 3', // Modified by Kenny 2020-09-05
  // 'orders' => 'Orders',
  'orders' => 'View Orders', // Modified by Kenny 2020-09-05
  'promote' => 'Promote',
  'save_changes' => 'Save Changes',
  'save_details' => 'Save Details',
  // 'service_fees' => 'Service Fees',
  'service_fees' => 'Step 6', // Modified by Kenny 2020-09-05
  // 'social' => 'Social',
  'social' => 'Step 4', // Modified by Kenny 2020-09-05
  'submit' => 'Submit',
  'success' => 'Success',
  // 'ticket_design' => 'Sign-Up Design',
  'ticket_design' => 'Step 7', // Modified by Kenny 2020-09-05
  'access_codes' => 'Access Codes',
  // 'tickets' => 'Sign-Ups',
  'tickets' => 'Edit Sign-Ups', // Modified by Kenny 2020-09-05
  'TOP' => 'TOP',
  'total' => 'total',
  'whoops' => 'Whoops!',
  'yes' => 'Yes',
  'no' => 'No',
  /*
   * Lines below will turn obsolete in localization helper, it is declared in app/Helpers/macros.
   * If you run it, it will break file input fields.
  */
  'upload' => 'Upload',
  'browse' => 'Browse',
  //================================== Obsolete strings ==================================//
  'LLH:obsolete' => [
    'months_long' => 'January|February|March|April|May|June|July|August|September|October|November|December',
  ],
);