function convertEventDateTime(oldDateTime,format) {
    var momentSingapore = moment.tz(oldDateTime, "Asia/Singapore");
    var momentLocal = momentSingapore.clone().tz(moment.tz.guess(true));
    switch (format) {
        case 'short':
            return momentLocal.format('D[<br/>]MMM[<br/>]YYYY');
            break;
        default:
            return momentLocal.format('dddd, YYYY-MM-DD HH:mm');
    }
}