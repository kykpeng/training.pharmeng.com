@extends('Shared.Layouts.Master')

@section('title')
    @parent
    @lang("Organiser.organiser_events")
@stop

@section('page_title')
    @lang("Organiser.organiser_name_events", ["name"=>$organiser->name])
@stop

@section('top_nav')
    @include('ManageOrganiser.Partials.TopNav')
@stop

@section('head')
    <style>
        .page-header {
            display: none;
        }
    </style>
    <script>
        $(function () {
            $('.colorpicker').minicolors({
                changeDelay: 500,
                change: function () {
                    var replaced = replaceUrlParam('{{route('showOrganiserHome', ['organiser_id'=>$organiser->id])}}', 'preview_styles', encodeURIComponent($('#OrganiserPageDesign form').serialize()));
                    document.getElementById('previewIframe').src = replaced;
                }
            });

        });

        @include('ManageOrganiser.Partials.OrganiserCreateAndEditJS')
    </script>
@stop

@section('menu')
    @include('ManageOrganiser.Partials.Sidebar')
@stop

@section('page_header')

@stop

@section('content')
<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Role Management</h2>

        </div>

        <div class="pull-right">

        @can('role-create')

            <a class="btn btn-success" href="{{ URL::to('roles/create') }}"> Create New Role</a>

            @endcan

        </div>

    </div>

</div>


@if ($message = Session::get('success'))

    <div class="alert alert-success">

        <p>{{ $message }}</p>

    </div>

@endif


<table class="table table-bordered">

  <tr>

     <th>No</th>

     <th>Name</th>
     <th>Permissions</th>

     <th width="280px">Action</th>

  </tr>

    @foreach ($roles as $key => $role)

    <tr>

        <td>{{ $role->id }}</td>

        <td>{{ $role->name }}</td>
        <td>
                                    @foreach($role->permissions as $permission)
                                        <span class="label label-info">{{$permission->name}}</span>
                                    @endforeach
                                    </td>

        <td>

            <!-- <a class="btn btn-info" href="{{ URL::to('roles/show',$role->id) }}">Show</a> -->

            @can('role-edit')

                <a class="btn btn-primary" href="{{ URL::to('roles/'.$role->id.'/edit') }}">Edit</a>

            @endcan

            @can('role-delete')
            <form method="post" action="{{ URL::to('roles/destroy',$role->id) }}" accept-charset="UTF-8" style="display:inline">
                <!-- {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!} -->

                <input name="_method" type="hidden" value="POST">
                
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

    </form>
                <!-- {!! Form::close() !!} -->

            @endcan

        </td>

    </tr>

    @endforeach

</table>


{!! $roles->render() !!}


@endsection