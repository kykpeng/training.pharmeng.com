@if(session()->get('message'))
    <script>showMessage('{{\Session::get('message')}}');</script>
@endif

@if(config('attendize.google_analytics_id'))

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{config('attendize.google_analytics_id')}}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{{config('attendize.google_analytics_id')}}');
</script>

@endif
