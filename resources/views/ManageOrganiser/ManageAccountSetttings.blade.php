@extends('Shared.Layouts.Master')

@section('title')
    @parent
    @lang("Organiser.organiser_events")
@stop

@section('page_title')
    @lang("Organiser.organiser_name_events", ["name"=>$organiser->name])
@stop

@section('top_nav')
    @include('ManageOrganiser.Partials.TopNav')
@stop

@section('head')
    <style>
        .page-header {
            display: none;
        }
    </style>
    <script>
        $(function () {
            $('.colorpicker').minicolors({
                changeDelay: 500,
                change: function () {
                    var replaced = replaceUrlParam('{{route('showOrganiserHome', ['organiser_id'=>$organiser->id])}}', 'preview_styles', encodeURIComponent($('#OrganiserPageDesign form').serialize()));
                    document.getElementById('previewIframe').src = replaced;
                }
            });

        });

        @include('ManageOrganiser.Partials.OrganiserCreateAndEditJS')
    </script>
@stop

@section('menu')
    @include('ManageOrganiser.Partials.Sidebar')
@stop

@section('page_header')

@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                @if (Auth::user()->hasrole('Account Admin'))
                    <li class="active">
                        <a href="#general" data-toggle="tab">@lang("ManageAccount.general")</a>
                    </li>
                    <li>
                        <a href="#payment" data-toggle="tab">@lang("ManageAccount.payment")</a>
                    </li>
                    <li>
                        <a href="#user" data-toggle="tab">@lang("ManageAccount.users")</a>
                    </li>
                    <li>
                        <a href="#roles" data-toggle="tab">Roles</a>
                    </li>
                    <li>
                        <a href="#aboutus" data-toggle="tab">@lang("ManageAccount.about")</a>
                    </li>
                @else
                    <li class="active">
                        <a href="#user" data-toggle="tab">@lang("ManageAccount.users")</a>
                    </li>
                @endif
            </ul>
            <div class="tab-content panel">
                <div class="tab-pane  @if (Auth::user()->hasrole('Account Admin')) active @endif" id="general">
                    {!! Form::model($account, array('url' => route('postEditAccount'), 'class' => 'ajax ')) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('first_name', trans("ManageAccount.first_name"), array('class'=>'control-label required')) !!}
                                {!!  Form::text('first_name', Input::old('first_name'),
                            array(
                            'class'=>'form-control'
                            ))  !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('last_name', trans("ManageAccount.last_name"), array('class'=>'control-label required')) !!}
                                {!!  Form::text('last_name', Input::old('last_name'),
                            array(
                            'class'=>'form-control'
                            ))  !!}
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('timezone_id', trans("ManageAccount.timezone"), array('class'=>'control-label required')) !!}
                                {!! Form::select('timezone_id', $timezones, $account->timezone_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('currency_id', trans("ManageAccount.default_currency"), array('class'=>'control-label required')) !!}
                                {!! Form::select('currency_id', $currencies, $account->currency_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-footer">
                                {!! Form::submit(trans("ManageAccount.save_account_details_submit"), ['class' => 'btn btn-success pull-right']) !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
                <div class="tab-pane scale_iframe" id="payment">
                    @include('ManageAccount.Partials.PaymentGatewayOptions')
                </div>
                <div class="tab-pane scale_iframe @if (!Auth::user()->hasrole('Account Admin')) active @endif" id="user">
                        {!! Form::open(array('url' => route('postInviteUser'), 'class' => 'ajax ')) !!}

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Programe</th>
                                    <th>Roles</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($account->availableUsers as $user)
                                    <tr>
                                        <td>
                                            {{$user->first_name}} {{$user->last_name}}
                                        </td>
                                        <td>
                                            {{$user->email}}
                                        </td>
                                        <td>
                                            @if($user->is_registered)
                                                <span class="label label-success">@lang("User.is_registered")</span>
                                            @endif
                                            @if($user->is_confirmed)
                                                <span class="label label-success">@lang("User.is_confirmed")</span>
                                            @endif
                                            @if($user->is_parent)
                                                <span class="label label-success">@lang("User.is_parent")</span>
                                            @endif
                                        </td>
                                        <td>
                                            @foreach($user->organisers as $organiser)
                                                <span class="label label-success">{{ $organiser->name}}</span>
                                                <br>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($user->roles as $role)
                                                <span class="label label-success">{{ $role->name }}</span>
                                                <br>
                                            @endforeach
                                        </td>
                                        <!-- <td>
                                            {!! $user->is_parent ? '<span class="label label-info">'.trans("ManageAccount.accout_owner").'</span>' : '' !!}
                                        </td> -->
                                        <td>
                                            <a class="btn btn-primary" href="{{ URL::to('user/'.$user->id.'/edit') }}">Edit</a>
                                            {{-- <button class="btn btn-primary" type="button">
                                            <a data-href="{{route('editUser', array('user_id' => $user->id))}}" data-modal-id="EditUser"
                                            class="loadModal editUserModal" href="javascript:void(0);"
                                            style="color:#fff">Edit</a>
                                            </button> --}}
                                            <button class="btn btn-danger" type="button">
                                            <a data-href="" data-modal-id="EditUser"
                                            class="loadModal deleteUserModal" href="javascript:void(0);"
                                            style="color:#fff">Delete</a>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3">
                                        <div class="input-group">
                                            {!! Form::text('email', '',  ['class' => 'form-control', 'placeholder' => trans("ManageAccount.email_address_placeholder")]) !!}
                                            <span class="input-group-btn">
                                                  {!!Form::submit(trans("ManageAccount.add_user_submit"), ['class' => 'btn btn-primary'])!!}
                                            </span>
                                        </div>
                                        <span class="help-block">
                                            @lang("ManageAccount.add_user_help_block")
                                        </span>
                                    </td>

                                </tr>

                                </tbody>
                            </table>
                        </div>
                        {!! Form::close() !!}
                </div>
                <div class="tab-pane scale_iframe" id="roles">
                 <div class="pull-right">

        @can('role-create')

            <a class="btn btn-success" href="{{ URL::to('roles/create') }}"> Create New Role</a>
                                
            @endcan

        </div>

                    <div class="table-responsive" style="margin-top:50px;">
                        {{-- @include('roles.index') --}}
                        <table class="table table-bordered">
                            <thead>
                                <th>Role Name</th>
                                <th width="70%">Permissions</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($roles as $role)
                                <tr>
                                    <td>
                                        {{$role->name}}
                                    </td>
                                    <td>
                                    @foreach($role->permissions as $permission)
                                        <span class="label label-info">{{$permission->name}}</span>
                                    @endforeach
                                    </td>
                                    <td>
                                    @can('role-edit')

<a class="btn btn-primary" href="{{ URL::to('roles/'.$role->id.'/edit') }}">Edit</a>

@endcan
@can('role-delete')
            <form method="post" action="{{ URL::to('roles/destroy',$role->id) }}" accept-charset="UTF-8" style="display:inline">
                <!-- {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!} -->

                <input name="_method" type="hidden" value="POST">
                
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

    </form>
                <!-- {!! Form::close() !!} -->

            @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane scale_iframe" id="aboutus">
                    <h4>
                        @lang("ManageAccount.version_info")
                    </h4>
                    <p>
                        @if($version_info['is_outdated'])
                            @lang("ManageAccount.version_out_of_date", ["installed" => $version_info['installed'], "latest"=> $version_info['latest'], "url"=>"https://attendize.com/documentation.php#download"]).
                        @else
                            @lang("ManageAccount.version_up_to_date", ["installed" => $version_info['installed']])
                        @endif
                    </p>
                    <h4>
                        {!! @trans("ManageAccount.licence_info") !!}
                    </h4>
                    <p>
                        {!! @trans("ManageAccount.licence_info_description") !!}
                    </p>
                    <h4>
                        {!! @trans("ManageAccount.open_source_soft") !!} Open-source Software
                    </h4>
                    <p>
                        {!! @trans("ManageAccount.open_source_soft_description") !!}
                    </p>
                </div>
            </div>
        </div>
@stop
