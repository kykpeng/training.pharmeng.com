<aside class="sidebar sidebar-left sidebar-menu">
    <section class="content">
        <h5 class="heading">{{ @trans("Organiser.organiser_menu", ["name"=>$organiser->name]) }}
            
        <ul id="nav" class="topmenu">
            <li class="{{ Request::is('*dashboard*') ? 'active' : '' }}">
                <a href="{{route('showOrganiserDashboard', array('organiser_id' => $organiser->id))}}">
                    <span class="figure"><i class="ico-home2"></i></span>
                    <span class="text">@lang("Organiser.dashboard")</span>
                </a>
            </li>
            <li class="{{ Request::is('*events*') ? 'active' : '' }}">
                <a href="{{route('showOrganiserEvents', array('organiser_id' => $organiser->id))}}">
                    <span class="figure"><i class="ico-calendar"></i></span>
                    <span class="text">@lang("Organiser.event")</span>
                </a>
            </li>

            <li class="{{ Request::is('*customize*') ? 'active' : '' }}">
                <a href="{{route('showOrganiserCustomize', array('organiser_id' => $organiser->id))}}">
                    <span class="figure"><i class="ico-cog"></i></span>
                    <span class="text">@lang("Organiser.customize")</span>
                </a>
            </li>

            <!-- <li class="{{ Request::is('*account_settings_page*') ? 'active' : '' }}">
                <a href="{{route('showAccountSettingsPage', array('organiser_id' => $organiser->id))}}">
                    <span class="figure"><i class="ico-cog"></i></span>
                    <span class="text">@lang("Top.account_settings")</span>
                </a>
            </li> -->

            <li>
                <a data-toggle="collapse" href="#admin-sub-menu" role="button" aria-expanded="true">
                    <span class="figure"><i class="ico-user"></i></span>
                    <span class="text">Admin</span>
                </a>


                <ul class="{{  Request::is('*account_settings_page*') ? 'child_menu collapse in' : 'child_menu collapse' }}" id="admin-sub-menu">

                    @if (Auth::user()->hasrole('Account Admin') || Auth::user()->is_admin == 1)
                    
                    <li class="{{ Request::is('*account_settings_page*') ? 'active' : '' }}">
                        <a href="{{route('showAccountSettingsPage', array('organiser_id' => $organiser->id))}}">
                            <span class="figure"><i class="ico-cog"></i></span>
                            <span class="text">@lang("Top.account_settings")</span>
                        </a>
                    </li>

                    @endif

                    @if (Auth::user()->hasrole('Account Admin'))
                        
                    <li>
                        <a href="#">
                            <span class="figure"><i class="ico-key"></i></span>
                            <span class="text">Sysadmin</span>
                        </a>
                    </li>

                    @endif
                    
                </ul>
            </li>
            
            
        </ul>
    </section>
</aside>
