{{--
Modified by Kenny 2020-06-14 to add Local vs. timezone changes
These are implemented in ManageEvent.Partials.EditEventForm and .CreateEvent
Also add CKEDITOR
--}}
{!! HTML::script('vendor/moment/moment.js')!!}
{!! HTML::script('vendor/moment-timezone/builds/moment-timezone-with-data.min.js')!!}
{!! HTML::script('vendor/ckeditor/ckeditor.js') !!}

<script>
    var localLang = navigator.languages ? navigator.languages : navigator.language;
    var localDateTimeFormat = moment().locale(localLang).localeData()._longDateFormat['LLL'];
    
    function getTimezoneString(dt) 
    { 
        return /\((.*)\)/.exec(dt.toString())[1];
    }

    function startdateLoad()
    {
        var dateCurrentTimezone = new Date();
        
        document.getElementById('startdateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
        
        if (document.getElementById('start_date').value)
        {
            var momentSingapore = moment.tz(document.getElementById('start_date').value, "Asia/Singapore");
            var momentLocal = momentSingapore.clone().tz(moment.tz.guess(true));
            var momentNewDelhi = momentSingapore.clone().tz("Asia/Colombo");
            var momentMadrid = momentSingapore.clone().tz("Europe/Madrid");
            var momentToronto = momentSingapore.clone().tz("America/Toronto");

            document.getElementById('start_date_local').value = momentLocal.format('YYYY-MM-DD HH:mm');
            document.getElementById('startdateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
            document.getElementById('startdateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
            document.getElementById('startdateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
            document.getElementById('startdateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
        }
    }

    function startdateChange()
    {
        var dateCurrentTimezone = new Date();
        var momentLocal = moment.tz(document.getElementById('start_date_local').value, moment.tz.guess(true));
        var momentSingapore = momentLocal.clone().tz("Asia/Singapore");
        var momentNewDelhi = momentLocal.clone().tz("Asia/Colombo");
        var momentMadrid = momentLocal.clone().tz("Europe/Madrid");
        var momentToronto = momentLocal.clone().tz("America/Toronto");

        document.getElementById('start_date').value = momentSingapore.format('YYYY-MM-DD HH:mm');
        document.getElementById('startdateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
        document.getElementById('startdateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
        document.getElementById('startdateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
        document.getElementById('startdateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
        document.getElementById('startdateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
    }

    function enddateLoad()
    {
        var dateCurrentTimezone = new Date();
        document.getElementById('enddateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
        
        if (document.getElementById('end_date').value)
        {
            var momentSingapore = moment.tz(document.getElementById('end_date').value, "Asia/Singapore");
            var momentLocal = momentSingapore.clone().tz(moment.tz.guess(true));
            var momentNewDelhi = momentSingapore.clone().tz("Asia/Colombo");
            var momentMadrid = momentSingapore.clone().tz("Europe/Madrid");
            var momentToronto = momentSingapore.clone().tz("America/Toronto");

            document.getElementById('end_date_local').value = momentLocal.format('YYYY-MM-DD HH:mm');
            document.getElementById('enddateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
            document.getElementById('enddateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
            document.getElementById('enddateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
            document.getElementById('enddateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
        }
    }

    function enddateChange()
    {
        var dateCurrentTimezone = new Date();
        var momentLocal = moment.tz(document.getElementById('end_date_local').value, moment.tz.guess(true));
        var momentSingapore = momentLocal.clone().tz("Asia/Singapore");
        var momentNewDelhi = momentLocal.clone().tz("Asia/Colombo");
        var momentMadrid = momentLocal.clone().tz("Europe/Madrid");
        var momentToronto = momentLocal.clone().tz("America/Toronto");

        document.getElementById('end_date').value = momentSingapore.format('YYYY-MM-DD HH:mm');
        document.getElementById('enddateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
        document.getElementById('enddateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
        document.getElementById('enddateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
        document.getElementById('enddateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
        document.getElementById('enddateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
    }

    function toggleLocationForm() {
        var x = document.getElementById("event_choose_address");

        switch (x.value) {
            case 'address-manual':
                document.getElementById('address-zoom').style.display = "none";
                document.getElementById('address-automatic').style.display = "none";
                document.getElementById('address-manual').style.display = "block";
                break;
            case 'address-automatic':
                document.getElementById('address-zoom').style.display = "none";
                document.getElementById('address-automatic').style.display = "block";
                document.getElementById('address-manual').style.display = "none";
                break;
            case 'address-zoom':
                document.getElementById('address-zoom').style.display = "block";
                document.getElementById('address-automatic').style.display = "none";
                document.getElementById('address-manual').style.display = "none";
                break;
            default:
                // Do nothing
                break;
        }
    }

    function toggleAdministrator() {
        if (document.getElementById('zoom_meeting_id').value) {
            alert("NOTE: Changing the Administrator will also change the Zoom Info.  If you have already sent Zoom Info to participants," +
                  "you will have to inform all participants of the change.");
            
            document.getElementById('zoom_url').value = '(You have changed the Administrator.  A new Zoom Invitation Text will be automatically generated after you click "Save".)';
        }
    }
</script>
{{-- End Modification --}}

<script>
    $(function() {
        try {
            $(".geocomplete").geocomplete({
                    details: "form.gf",
                    types: ["geocode", "establishment"]
                }).bind("geocode:result", function(event, result) {
                    console.log(result);
            }, 1000);

        } catch (e) {
            // console.log(e);
        }

        $("#DatePicker").remove();
        var $div = $("<div>", {id: "DatePicker"});
        $("body").append($div);
        $div.DateTimePicker({
            dateTimeFormat: window.Attendize.DateTimeFormat,
            dateSeparator: window.Attendize.DateSeparator
        });

        // Modified by Kenny 2020-09-04
        CKEDITOR.replace('description', {height: 200});
        if (document.getElementById("outline")) {
            CKEDITOR.replace('outline', {height: 100});
            CKEDITOR.replace('outcome', {height: 100});
            CKEDITOR.replace('about_instructor', {height: 100});
        }

        startdateLoad();
        enddateLoad();

        $('[data-toggle="tooltip"]').tooltip();        
        // End Modificaiton

    });
</script>

<style>
    .editor-toolbar {
        border-radius: 0 !important;
    }
    .CodeMirror, .CodeMirror-scroll {
        min-height: 100px !important;
    }

    .create_organiser, .address-manual, .address-automatic, .address-zoom {
        padding: 10px;
        border: 1px solid #ddd;
        margin-top: 10px;
        margin-bottom: 10px;
        background-color: #FAFAFA;
    }

    .in-form-link {
        display: block; padding: 5px;margin-bottom: 5px;padding-left: 0;
    }
</style>
