{!! HTML::script('vendor/moment/moment.js')!!}
{!! HTML::script('vendor/moment-timezone/builds/moment-timezone-with-data.min.js')!!}
{!! HTML::script('vendor/ckeditor/ckeditor.js') !!}


<script>
    var localLang = navigator.languages ? navigator.languages : navigator.language;
    var localDateTimeFormat = moment().locale(localLang).localeData()._longDateFormat['LLL'];
    
    function getTimezoneString(dt) 
    { 
        return /\((.*)\)/.exec(dt.toString())[1];
    }

    function startdateLoad()
    {
        var dateCurrentTimezone = new Date();
        
        document.getElementById('startdateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
        
        if (document.getElementById('start_sale_date').value)
        {
            var momentSingapore = moment.tz(document.getElementById('start_sale_date').value, "Asia/Singapore");
            var momentLocal = momentSingapore.clone().tz(moment.tz.guess(true));
            var momentNewDelhi = momentSingapore.clone().tz("Asia/Colombo");
            var momentMadrid = momentSingapore.clone().tz("Europe/Madrid");
            var momentToronto = momentSingapore.clone().tz("America/Toronto");

            document.getElementById('start_date_local').value = momentLocal.format('YYYY-MM-DD HH:mm');
            document.getElementById('startdateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
            document.getElementById('startdateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
            document.getElementById('startdateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
            document.getElementById('startdateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
        }
    }

    function startdateChange()
    {
        var dateCurrentTimezone = new Date();
        if (document.getElementById('end_date_local').value) {
            var momentLocal = moment.tz(document.getElementById('start_date_local').value, moment.tz.guess(true));
            var momentSingapore = momentLocal.clone().tz("Asia/Singapore");
            var momentNewDelhi = momentLocal.clone().tz("Asia/Colombo");
            var momentMadrid = momentLocal.clone().tz("Europe/Madrid");
            var momentToronto = momentLocal.clone().tz("America/Toronto");

            document.getElementById('start_sale_date').value = momentSingapore.format('YYYY-MM-DD HH:mm');
            document.getElementById('startdateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
            document.getElementById('startdateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
            document.getElementById('startdateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
            document.getElementById('startdateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
            document.getElementById('startdateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
        } else {
            var momentSingapore = '-';
            var momentNewDelhi = '-';
            var momentMadrid = '-';
            var momentToronto = '-';

            document.getElementById('start_sale_date').value = '';
            document.getElementById('startdateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
            document.getElementById('startdateTimeSingapore').innerHTML = '';
            document.getElementById('startdateTimeNewDelhi').innerHTML = '';
            document.getElementById('startdateTimeMadrid').innerHTML = '';
            document.getElementById('startdateTimeToronto').innerHTML = '';
        }
    }

    function enddateLoad()
    {
        var dateCurrentTimezone = new Date();
        document.getElementById('enddateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
        
        if (document.getElementById('end_sale_date').value)
        {
            var momentSingapore = moment.tz(document.getElementById('end_sale_date').value, "Asia/Singapore");
            var momentLocal = momentSingapore.clone().tz(moment.tz.guess(true));
            var momentNewDelhi = momentSingapore.clone().tz("Asia/Colombo");
            var momentMadrid = momentSingapore.clone().tz("Europe/Madrid");
            var momentToronto = momentSingapore.clone().tz("America/Toronto");

            document.getElementById('end_date_local').value = momentLocal.format('YYYY-MM-DD HH:mm');
            document.getElementById('enddateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
            document.getElementById('enddateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
            document.getElementById('enddateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
            document.getElementById('enddateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
        }
    }

    function enddateChange()
    {
        var dateCurrentTimezone = new Date();
        if (document.getElementById('end_date_local').value) {
            var momentLocal = moment.tz(document.getElementById('end_date_local').value, moment.tz.guess(true));
            var momentSingapore = momentLocal.clone().tz("Asia/Singapore");
            var momentNewDelhi = momentLocal.clone().tz("Asia/Colombo");
            var momentMadrid = momentLocal.clone().tz("Europe/Madrid");
            var momentToronto = momentLocal.clone().tz("America/Toronto");

            document.getElementById('end_sale_date').value = momentSingapore.format('YYYY-MM-DD HH:mm');
            document.getElementById('enddateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
            document.getElementById('enddateTimeSingapore').innerHTML = momentSingapore.format(localDateTimeFormat);
            document.getElementById('enddateTimeNewDelhi').innerHTML = momentNewDelhi.format(localDateTimeFormat);
            document.getElementById('enddateTimeMadrid').innerHTML = momentMadrid.format(localDateTimeFormat);
            document.getElementById('enddateTimeToronto').innerHTML = momentToronto.format(localDateTimeFormat);
        } else {
            var momentSingapore = '-';
            var momentNewDelhi = '-';
            var momentMadrid = '-';
            var momentToronto = '-';

            document.getElementById('end_sale_date').value = '';
            document.getElementById('enddateCurrentTimezone').innerHTML = getTimezoneString(dateCurrentTimezone);
            document.getElementById('enddateTimeSingapore').innerHTML = momentSingapore;
            document.getElementById('enddateTimeNewDelhi').innerHTML = momentNewDelhi;
            document.getElementById('enddateTimeMadrid').innerHTML = momentMadrid;
            document.getElementById('enddateTimeToronto').innerHTML = momentToronto;
        }

    }

</script>
<script>
    $(function() {
        $("#DatePicker").remove();
        var $div = $("<div>", {id: "DatePicker"});
        $("body").append($div);
        $div.DateTimePicker({
            dateTimeFormat: window.Attendize.DateTimeFormat,
            dateSeparator: window.Attendize.DateSeparator
        });

        startdateLoad();
        enddateLoad();

        $('[data-toggle="tooltip"]').tooltip();        
    });
</script>

