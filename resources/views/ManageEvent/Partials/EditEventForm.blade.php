@include('ManageOrganiser.Partials.EventCreateAndEditJS')

{!! Form::model($event, array('url' => route('postEditEvent', ['event_id' => $event->id]), 'class' => 'ajax gf', 'id' => 'eventForm', 'name' => 'eventForm')) !!}

<div class="row">
    <div class="col-md-12">
        <p>
            <strong>Instructions:</strong>Items marked with <font color="#FF0000">*</font> are required.  For other items, you may leave them empty,
            or the more you fill out the more it will help visitors understand your offering.
        </p>

        <div class="form-group">
            {!! Form::label('instructor_id', trans("Event.event_instructor"), array('class'=>'control-label')) !!}
            {{-- {!! Form::select('instructor_id', $instructors, $event->currency_id, ['class' => 'form-control']) !!} --}}
            {!! Form::select('instructor_id', $instructors, $event->instructor_id, ['class' => 'form-control', 'placeholder' => 'Please select ...']) !!}
            <div class="custom-checkbox mb5">
                {!! Form::checkbox('bcc_instructor', 1, $event->bcc_instructor, ['id' => 'bcc_instructor', 'data-toggle' => 'toggle']) !!}
                {!! Form::label('bcc_instructor', 'bcc notify me when a participant registers') !!}
            </div>
        </div>
  
        <div class="form-group">
            {!! Form::label('administrator_id', trans("Event.event_administrator"), array('class'=>'control-label')) !!}
            <span class="figure"><i class="ico-question4" data-toggle="tooltip" data-placement="top" title="{!! trans("Event.event_administrator_tooltip") !!}"></i></span>
            {!! Form::select('administrator_id', $administrators, $event->administrator_id, ['class' => 'form-control', 'placeholder' => 'Please select ...', 'onchange'=>"toggleAdministrator()"]) !!}
            <div class="custom-checkbox mb5">
                {!! Form::checkbox('bcc_administrator', 1, $event->bcc_administrator, ['id' => 'bcc_administrator', 'data-toggle' => 'toggle']) !!}
                {!! Form::label('bcc_administrator', 'bcc notify me when a participant registers') !!}
            </div>
        </div>
  
        <div class="form-group">
            {!! Form::label('title', trans("Event.event_title"), array('class'=>'control-label required')) !!}
            {!!  Form::text('title', Input::old('title'),
                                        array(
                                        'class'=>'form-control',
                                        'placeholder'=>trans("Event.event_title_placeholder", ["name"=>Auth::user()->first_name])
                                        ))  !!}
        </div>

        {{-- Modified by Kenny 2020-09-04 --}}
        <div class="form-group">
            {!! Form::label('objective', trans("Event.event_objective"), array('class'=>'control-label')) !!}
            {!!  Form::textarea('objective', Input::old('objective'),
                                        array(
                                        'class'=>'form-control',
                                        'rows' => 3,
                                        'placeholder'=>trans("Event.event_objective_placeholder", ["name"=>Auth::user()->first_name])
                                        ))  !!}
        </div>
        {{-- End Modification --}}

        <div class="form-group">
           {!! Form::label('description', trans("Event.event_description"), array('class'=>'control-label required')) !!}
            {!!  Form::textarea('description', Input::old('description'),
                                        array(
                                        'class'=>'form-control',
                                        'rows' => 5,
                                        'placeholder'=>trans("Event.event_description_placeholder", ["name"=>Auth::user()->first_name])
                                        ))  !!}
        </div>

        {{-- Modified by Kenny 2020-09-04 --}}
        <div class="form-group">
            {!! Form::label('outline', trans("Event.event_outline"), array('class'=>'control-label')) !!}
            {!!  Form::textarea('outline', Input::old('outline'),
                                        array(
                                        'class'=>'form-control',
                                        'rows' => 5,
                                        'placeholder'=>trans("Event.event_outline_placeholder", ["name"=>Auth::user()->first_name])
                                        ))  !!}
        </div>

        <div class="form-group">
            {!! Form::label('outcome', trans("Event.event_outcome"), array('class'=>'control-label')) !!}
            {!!  Form::textarea('outcome', Input::old('outcome'),
                                        array(
                                        'class'=>'form-control',
                                        'rows' => 5,
                                        'placeholder'=>trans("Event.event_outcome_placeholder", ["name"=>Auth::user()->first_name])
                                        ))  !!}
        </div>

        <div class="form-group">
            {!! Form::label('about_instructor', trans("Event.event_about_instructor"), array('class'=>'control-label')) !!}
            {!!  Form::textarea('about_instructor', Input::old('about_instructor'),
                                        array(
                                        'class'=>'form-control',
                                        'rows' => 5,
                                        'placeholder'=>trans("Event.event_about_instructor_placeholder", ["name"=>Auth::user()->first_name])
                                        ))  !!}
        </div>
        {{-- End Modification --}}

        {{--
        Modified by Kenny 2020-06-28, add a drop-down box to select physical address or online address
        --}}
        <div class="address-choice">
            {!! Form::label('event_choose_address', trans("Event.event_choose_address"), array('class'=>'control-label')) !!}
            @php
            switch ($event->location_is_manual) {
                case 0:
                    $event_choose_address = 'address-automatic';
                    break;
                case 1:
                    $event_choose_address = 'address-manual';
                    break;
                case 2:
                    $event_choose_address = 'address-zoom';
                    break;
                default:
                    // Do nothing
                    break;
            }
            @endphp
            {!! Form::select('event_choose_address', [
                    'address-manual' => trans("Event.event_choose_address_manual"),
                    'address-automatic' => trans("Event.event_choose_address_automatic"),
                    'address-zoom' => trans("Event.event_choose_address_zoom"),
                ],
                $event_choose_address,
                ['class'=>'form-control',
                 'onchange'=>"toggleLocationForm()"
                ]
            ) !!}
        </div>

        {{--
        <div class="address-zoom" style="display:{{$event->location_is_manual == 2 ? 'block' : 'none'}};">
        --}}
        <div class="address-zoom" id="address-zoom" style="display:{{$event->location_is_manual == 2 ? 'block' : 'none'}};">
            <div class="form-group">
                {!! Form::label('zoom_venue_name', trans("Event.venue_name"), array('class'=>'control-label required ')) !!}
                {!!  Form::text('zoom_venue_name', $event->venue_name, [
                                        'class'=>'form-control location_field',
                                        'placeholder'=>'Zoom', // same as above
                                        'disabled'=>'disabled'
                            ])  !!}
            </div>
            <div class="form-group">
                {!! Form::label('zoom_url', trans("Event.zoom_url"), array('class'=>'control-label')) !!}
                <span class="figure"><i class="ico-question4" data-toggle="tooltip" data-placement="top" title="{!! trans("Event.zoom_url_tooltip") !!}"></i></span>
                {!!  Form::textarea('zoom_url', $event->location_address_line_1, [
                                        'class'=>'form-control location_field',
                                        'rows' => 10,
                                        'placeholder'=>'(No need to edit.  A Zoom Invitation Text will be automatically generated after you click "Save".)'//'E.g: https://zoom.us/12345.'
                            ])  !!}
                {!! Form::hidden('zoom_meeting_id', $event->location_address_line_2, ['class' => 'location_field', 'id' => 'zoom_meeting_id']) !!}
            </div>
        </div>
        {{-- End Modification --}}

        {{--
        Modified by Kenny 2020-06-28, 'location_is_manual' added more parameters
        <div class="form-group address-automatic" style="display:{{$event->location_is_manual ? 'none' : 'block'}};">
        --}}
        <div class="form-group address-automatic" id="address-automatic" style="display:{{$event->location_is_manual == 0 ? 'block' : 'none'}};">
        {{-- End Modification --}}
            {!! Form::label('name', trans("Event.venue_name"), array('class'=>'control-label required ')) !!}
            {!!  Form::text('venue_name_full', Input::old('venue_name_full'),
                                        array(
                                        'class'=>'form-control geocomplete location_field',
                                        'placeholder'=>trans("Event.venue_name_placeholder")//'E.g: The Crab Shack'
                                        ))  !!}

            <!--These are populated with the Google places info-->
            <div>
               {!! Form::hidden('formatted_address', $event->location_address, ['class' => 'location_field']) !!}
               {!! Form::hidden('street_number', $event->location_street_number, ['class' => 'location_field']) !!}
               {!! Form::hidden('country', $event->location_country, ['class' => 'location_field']) !!}
               {!! Form::hidden('country_short', $event->location_country_short, ['class' => 'location_field']) !!}
               {!! Form::hidden('place_id', $event->location_google_place_id, ['class' => 'location_field']) !!}
               {!! Form::hidden('name', $event->venue_name, ['class' => 'location_field']) !!}
               {!! Form::hidden('location', '', ['class' => 'location_field']) !!}
               {!! Form::hidden('postal_code', $event->location_post_code, ['class' => 'location_field']) !!}
               {!! Form::hidden('route', $event->location_address_line_1, ['class' => 'location_field']) !!}
               {!! Form::hidden('lat', $event->location_lat, ['class' => 'location_field']) !!}
               {!! Form::hidden('lng', $event->location_long, ['class' => 'location_field']) !!}
               {!! Form::hidden('administrative_area_level_1', $event->location_state, ['class' => 'location_field']) !!}
               {!! Form::hidden('sublocality', '', ['class' => 'location_field']) !!}
               {!! Form::hidden('locality', $event->location_address_line_1, ['class' => 'location_field']) !!}
            </div>
            <!-- /These are populated with the Google places info-->

        </div>

        <div class="address-manual" id="address-manual" style="display:{{$event->location_is_manual == 1 ? 'block' : 'none'}};">
            <div class="form-group">
                {!! Form::label('location_venue_name', trans("Event.venue_name"), array('class'=>'control-label required ')) !!}
                {!!  Form::text('location_venue_name', $event->venue_name, [
                                        'class'=>'form-control location_field',
                                        'placeholder'=>trans("Event.venue_name_placeholder") // same as above
                            ])  !!}
            </div>
            <div class="form-group">
                {!! Form::label('location_address_line_1', trans("Event.address_line_1"), array('class'=>'control-label')) !!}
                {!!  Form::text('location_address_line_1', $event->location_address_line_1, [
                                        'class'=>'form-control location_field',
                                        'placeholder'=>trans("Event.address_line_1_placeholder")//'E.g: 45 Grafton St.'
                            ])  !!}
            </div>
            <div class="form-group">
                {!! Form::label('location_address_line_2', trans("Event.address_line_2"), array('class'=>'control-label')) !!}
                {!!  Form::text('location_address_line_2', $event->location_address_line_2, [
                                        'class'=>'form-control location_field',
                                        'placeholder'=>trans("Event.address_line_2_placeholder")//'E.g: Dublin.'
                            ])  !!}
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('location_state', trans("Event.city"), array('class'=>'control-label')) !!}
                        {!!  Form::text('location_state', $event->location_state, [
                                        'class'=>'form-control location_field',
                                        'placeholder'=>trans("Event.city_placeholder")//'E.g: Dublin.'
                            ])  !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('location_post_code', trans("Event.post_code"), array('class'=>'control-label')) !!}
                        {!!  Form::text('location_post_code', $event->location_post_code, [
                                        'class'=>'form-control location_field',
                                        'placeholder'=>trans("Event.post_code_placeholder")// 'E.g: 94568.'
                            ])  !!}
                    </div>
                </div>
            </div>
        </div>

        {{-- 
        Modified by Kenny 2020-06-28
        Disable this section
        
        <div class="clearfix" style="margin-top:-10px; padding: 5px; padding-top: 0px;">
            <span class="pull-right">
                @lang("Event.or(manual/existing_venue)") <a data-clear-field=".location_field" data-toggle-class=".address-automatic, .address-manual" data-show-less-text="{{$event->location_is_manual ? trans("Event.enter_manual"):trans("Event.enter_existing")}}" href="javascript:void(0);" class="show-more-options clear_location">{{$event->location_is_manual ? trans("Event.enter_existing"):trans("Event.enter_manual")}}</a>
            </span>
        </div>

        --}}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('start_date', trans("Event.event_start_date"), array('class'=>'required control-label')) !!}
                    {{--
                    Modified by Kenny 2020-06-14 added mutli-timezone functions and displays
                    Refer to JavaScript added in ManageOrganiser.Partials.EventCreateAndEditJS
                    {!!  Form::text('start_date', $event->getFormattedDate('start_date'),
                                                [
                                            'class'=>'form-control start hasDatepicker ',
                                            'data-field'=>'datetime',
                                            'data-startend'=>'start',
                                            'data-startendelem'=>'.end',
                                            'readonly'=>''
                                        ])  !!}
                     --}}
                    {!!  Form::hidden('start_date', $event->getFormattedDate('start_date')) !!}
                    {!!  Form::text('start_date_local', '',
                                                [
                                                    'class'=>'form-control start hasDatepicker ',
                                                    'data-field'=>'datetime',
                                                    'data-startend'=>'start',
                                                    'data-startendelem'=>'.end',
                                                    'readonly'=>'',
                                                    'onchange'=>'startdateChange()',
                                                    'onload'=>'startdateLoad()',
                                                    'name'=>'',
                                                    'id'=>'start_date_local',
                                                ])  !!}
                    <small>
                        <p>Date/Time above is shown in your current timezone: <strong><span id="startdateCurrentTimezone">?</span></strong>.  Equivalent to:</p>
                        <ul>
                            <li>New York: <span id="startdateTimeToronto">-</span></li>
                            <li>Barcelona: <span id="startdateTimeMadrid">-</span></li>
                            <li>Singapore: <span id="startdateTimeSingapore">-</span></li>
                            <li>New Delhi: <span id="startdateTimeNewDelhi">-</span></li>
                        </ul>
                    </small>
                    {{-- End Modification --}}
                </div>
            </div>

            <div class="col-sm-6 ">
                <div class="form-group">
                    {!!  Form::label('end_date', trans("Event.event_end_date"),
                                        [
                                    'class'=>'required control-label '
                                ])  !!}
                    {{--
                    Modified by Kenny 2020-06-14 added mutli-timezone functions and displays
                    Refer to JavaScript added in ManageOrganiser.Partials.EventCreateAndEditJS
                    {!!  Form::text('end_date', $event->getFormattedDate('end_date'),
                                                [
                                            'class'=>'form-control end hasDatepicker ',
                                            'data-field'=>'datetime',
                                            'data-startend'=>'end',
                                            'data-startendelem'=>'.start',
                                            'readonly'=>''
                                        ])  !!}
                     --}}
                    {!!  Form::hidden('end_date', $event->getFormattedDate('end_date')) !!}
                    {!!  Form::text('end_date_local', '',
                                                [
                                                    'class'=>'form-control end hasDatepicker ',
                                                    'data-field'=>'datetime',
                                                    'data-startend'=>'end',
                                                    'data-startendelem'=>'.start',
                                                    'readonly'=>'',
                                                    'onchange'=>'enddateChange()',
                                                    'onload'=>'enddateLoad()',
                                                    'name'=>'',
                                                    'id'=>'end_date_local',
                                                ])  !!}
                    <small>
                        <p>Date/Time above is shown in your current timezone: <strong><span id="enddateCurrentTimezone">?</span></strong>.  Equivalent to:</p>
                        <ul>
                            <li>New York: <span id="enddateTimeToronto">-</span></li>
                            <li>Barcelona: <span id="enddateTimeMadrid">-</span></li>
                            <li>Singapore: <span id="enddateTimeSingapore">-</span></li>
                            <li>New Delhi: <span id="enddateTimeNewDelhi">-</span></li>
                        </ul>
                    </small>
                    {{-- End Modification --}}
                </div>
            </div>
        </div>


    </div>

    <div class="col-md-12">
        <div class="panel-footer mt15 text-right">
           {!! Form::hidden('organiser_id', $event->organiser_id) !!}
           {!! Form::submit(trans("Event.save_changes"), ['class'=>"btn btn-success"]) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>

