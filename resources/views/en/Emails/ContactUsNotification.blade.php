@extends('en.Emails.Layouts.Master')

@section('message_content')
<p>
You received a message from:
</p>
<p>
    <strong>Name:</strong><br/>{{ $name }}
</p>
<p>
    <strong>Email:</strong><br/>{{ $email }}
</p>
<p>
    <strong>Subject:</strong><br/>{{ $subject }}
</p>
<p>
    <strong>Message:</strong><br/>{!! Markdown::parse($message_body) !!}
</p>
@stop