@if (!empty($event->outcome))
    <section id="outcome" class="container">
        <div class="row">
            <h1 class="section_head">
                @lang("Public_ViewEvent.event_outcome")
            </h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content event_details" property="description">
                    {!! Markdown::parse($event->outcome) !!}
                </div>
            </div>
        </div>
    </section>
@endif