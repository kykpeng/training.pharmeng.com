@if (!empty($event->about_instructor))
    <section id="about_instructor" class="container">
        <div class="row">
            <h1 class="section_head">
                @lang("Public_ViewEvent.event_about_instructor")
            </h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content event_details" property="description">
                    {!! Markdown::parse($event->about_instructor) !!}
                </div>
            </div>
        </div>
    </section>
@endif