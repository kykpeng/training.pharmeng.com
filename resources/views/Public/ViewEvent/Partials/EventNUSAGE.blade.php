{{-- NUSAGE-specific stylesheet --}}
{!!HTML::style(config('attendize.cdn_url_static_assets').'/assets/stylesheet/nusage.css')!!}

{{-- Add a Print Button hovering at the top --}}
<button type="button" class="donotprint btn btn-primary" onclick="window.print();return false;" style="
	height: 50px;
	left: 10%;
	position: fixed;
	top: 100px;
	width: 180px;">Print PDF Brochure</button>

{{-- Title Section --}}
<?php
	// PHP randomizing cover page background image
	$bg = array('NUSAGEbg1.jpg', 'NUSAGEbg2.jpg', 'NUSAGEbg3.jpg' ); // array of filenames

	$i = rand(0, count($bg)-1); // generate random number size of the array
	$selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen
?>

<section id="title_page" class="nusage_container printpagebreak nusage_title-page" style="background-image: url('{{asset('assets/images/'.$selectedBg)}}');">
	<div class="nusage_title-banner-top">
		Shaping Human Capital for Challenges in the Pharmaceutical Industry
	</div>

	<div class="donotdisplay" style="width:100%; position:relative; top:80mm; left: 0mm; margin: auto;">
		<img src="{{asset('assets/images/'.$selectedBg)}}" style="max-width: 100%; min-width: 100%;">
	</div>

	<div class="nusage_title-banner-middle">
		<img src="{{asset('assets/images/NUS-NUSAGE_logo landscape.png')}}" style="max-height:100%;">
		&nbsp;&nbsp;
		<img src="{{asset('assets/images/PharmEng_logo landscape.png')}}" style="max-height:100%; padding: 1mm;">
	</div>

	<div class="nusage_title-banner-bottom">
		www.PharmEng.asia | Copyright &copy; {{ now()->year }} PharmEng Technology
	</div>
</section>

{{-- Detailed Course Descriptions --}}
<section id="course_details" class="nusage_container_overflow printpagebreak">
	<div class="nusage_container_overflow_header donotprint" style="background-image: url('{{config('attendize.cdn_url_user_assets').'/'.$event->images->first()['image_path']}}');">
		<div class="nusage_header-banner">
			Course Details
		</div>
	</div>
	
	<br />
	<div class="nusage_details" property="description">
		<h1>Objective</h1>
		{!! Markdown::parse($event->objective) !!}
		<h1>Description</h1>
		{!! Markdown::parse($event->description) !!}
		<h1>Outline</h1>
		{!! Markdown::parse($event->outline) !!}
		<h1>Learning Outcome</h1>
		{!! Markdown::parse($event->outcome) !!}
	</div>
	<br />
	<br />

	<div class="nusage_container_overflow_footer donotprint" style="background-image: url('{{config('attendize.cdn_url_user_assets').'/'.$event->images->first()['image_path']}}');">
		&nbsp;
	</div>

</section>

{{-- Registration Section --}}
<section id="registrations" class="nusage_container printpagebreak">
	<div class="nusage_header_container">
		<img alt="NUSAGE Registration Page" src="{{asset('assets/images/NUSAGERegistrationPage.jpg')}}" property="image">
		<div class="nusage_header-banner">{{$event->title}}</div>
	</div>
	<br />

    <div class="pharmeng_about">
	    <h2 class="pharmeng_about2">
	        Course Registration
		</h2>
		
	    <p class="pharmeng_about2">To register for this course or for any other course enquiries, please contact:</p>
	    <p class="pharmeng_about2"><b>Ms. CHEW Ying Ying</b><br />
	    Senior Manager<br />
	    Office: S4A 03<br />
	    Tel: 65168977<br />
		<p class="pharmeng_about2">Email: <a href="mailto:phacyy@nus.edu.sg">phacyy@nus.edu.sg</a></p>
	</div>

	{{-- Placeholder to fix Div Minimum Height issue under nusage_container --}}
	<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />


	<div class="nusage_footer_container">
		<img alt="NUS Science Landscape" src="{{asset('assets/images/NUSScienceLandscape.jpg')}}" property="image">
		<img alt="NUSAGE Footer" src="{{asset('assets/images/NUSAGEAboutTrainingProviderFooter.jpg')}}" property="image">
		<div class="nusage_footer-logo">
			<img src="{{asset('assets/images/NUS-NUSAGE_logo landscape.png')}}" style="height:100%;">
			&nbsp;&nbsp;
			<img src="{{asset('assets/images/PharmEng_logo landscape.png')}}" style="height:100%; padding: 1mm;">
		</div>
		<div class="nusage_footer-banner">www.PharmEng.asia | Copyright &copy; {{ now()->year }} PharmEng Technology</div>

	</div>
</section>

{{-- PAGE 1 of About the Training Provider --}}
<section id="about_training_provider_1" class="nusage_container printpagebreak">
	<div class="nusage_header_container">
		<img alt="NUS Science Landscape" src="{{asset('assets/images/NUSAGEAboutTrainingProviderHeader.jpg')}}" property="image">
		<div class="nusage_header-banner">About the Training Provider</div>
	</div>
	<br />

	<div class="pharmeng_about">
		<h2 class="pharmeng_about2">
			PharmEng Technology
		</h2>
		<p class="pharmeng_about2">PharmEng Technology ("PharmEng"), a division of PE Pharma Inc., provides professional development and certification training programs throughout North America and Asia. We deliver over 35 courses to the pharmaceutical, biotechnology, nutraceutical and medical devices industries in the areas of:</p>
		
		<table width=100%  class="pharmeng_about2">
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">cGMPs</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Validation</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Engineering</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Project Management</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Medical Devices</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Quality Compliance</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Quality Assurance</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Regulatory Affairs</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Manufacturing</td>
			</tr>
		</table>

		<h2 class="pharmeng_about2">
			Why PharmEng Professional Training?
		</h2>
		<br />

		<table width=100%  class="pharmeng_about2">
			<tr>
				<td style="width:5mm; vertical-align:top;">■</td>
				<td style="vertical-align:top;">Unique curriculum that covers key areas critical to the success of the industry, through courses that integrate theory and practice</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">■</td>
				<td style="vertical-align:top;">Advisory committee that includes members from industry, academia and government, ensuring that important regulatory and industry issues are addressed</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">■</td>
				<td style="vertical-align:top;">Custom courses that cover both general and basic "know-how" as well as current challenges, issues and new developments in the industry</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">■</td>
				<td style="vertical-align:top;">Instructors that have been selected among industry leaders and subject experts who will provide challenging course work and valuable hands-on experience</td>
			</tr>
		</table>
		<br />

		<h2 class="pharmeng_about2">
			PharmEng delivers courses to two distinct groups:
		</h2>
		<br>
		
		<table width=100%  class="pharmeng_about2">
			<tr>
				<td style="width:5mm; vertical-align:top;">1.</td>
				<td style="vertical-align:top;"><b>Corporate Training:</b> Experienced industry professionals who require current best practices in order to keep up-to-date with industry standards, Good Manufacturing Practices (GMP's) and regulations.</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">2.</td>
				<td style="vertical-align:top;"><b>Career Training:</b> Next generation individuals seeking careers in the industry who need practical skills and "know-how" for the pharmaceutical and biotechnology workforce.</td>
			</tr>
		</table>
		<br />

		<p class="pharmeng_about2">For those individuals requiring one day professional development programs, courses are available through any of the PharmEng offices located throughout North America and Asia with access to course listings, course availability and registration through the PharmEng website www.pharmeng.com.</p>
	</div>

	<div class="nusage_footer_container">
		<img alt="NUSAGE Footer" src="{{asset('assets/images/NUSAGEAboutTrainingProviderFooter.jpg')}}" property="image">
		<div class="nusage_footer-logo">
			<img src="{{asset('assets/images/NUS-NUSAGE_logo landscape.png')}}" style="height:100%;">
			&nbsp;&nbsp;
			<img src="{{asset('assets/images/PharmEng_logo landscape.png')}}" style="height:100%; padding: 1mm;">
		</div>
		<div class="nusage_footer-banner">www.PharmEng.asia | Copyright &copy; {{ now()->year }} PharmEng Technology</div>
	</div>
</section>

{{-- PAGE 2 of About the Training Provider --}}
<section id="about_training_provider_2" class="nusage_container printpagebreak">
	<div class="nusage_header_container">
		<img alt="NUS Science Landscape" src="{{asset('assets/images/NUSAGEAboutTrainingProviderHeader.jpg')}}" property="image">
		<div class="nusage_header-banner">About the Training Provider</div>
	</div>
	<br />

	<div class="pharmeng_about">
		<h2 class="pharmeng_about2">
			Certificate Programs
		</h2>
		<p class="pharmeng_about2">For career training and certification, PharmEng offers programs through national and internationally-recognized universities delivering certificate programs such as:</p>
		<p class="pharmeng_about2">The Biopharmaceutical Technology Certificate Program for the University of Waterloo and the National Tsing Hua University College of Life Science in Taiwan</p>
		<p class="pharmeng_about2">The Biotechnology and Pharmaceutical Technology Program for Cape Breton University</p>

		<h2 class="pharmeng_about2">
			Instructors and Course Materials
		</h2>
		<p class="pharmeng_about2">All instructors are subject matter experts with direct industry experience. Instructors include guest speakers from industry, government and academia. Course materials are developed by PharmEng in-house and are constantly updated to keep current with the regulatory environment. As the industry changes, so do the issues and challenges.</p>
		<p class="pharmeng_about2">Our courses, with supporting materials, link together:</p>
		
		<table width=100%  class="pharmeng_about2">
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Training</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Regulations</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Government</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Industry</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">Academia</td>
			</tr>
			<tr>
				<td style="width:5mm; vertical-align:top;">✓</td>
				<td style="vertical-align:top;">International Standards</td>
			</tr>
		</table>

		<h2 class="pharmeng_about2">
			Conferences
		</h2>
		<p class="pharmeng_about2">PharmEng offers conferences throughout the year in collaboration with Health Canada, and various professional associations in biotechnology, pharmaceutical, medical devices, nutraceutical and healthcare industries.</p>
		<br />

		<table class="pharmeng_about2" style="width:100%;">
			<tr>
				<td style="width:5%; vertical-align:top;">
					<font size="48px">&ldquo;</font>
				</td>
				<td>
					<table class="pharmeng_about2" style="width:100%; padding:5mm;">
						<tr>
							<td style="width:40%;">
								<i>&ldquo;Best instructor and best coverage of this subject that I've experienced yet. Great session - so glad I came.&rdquo;</i>
								<br />
								<strong>- IMRIS Inc.</strong>
							</td>
							<td style="width:20%;">
								&nbsp;
							</td>
							<td style="width:40%;">
								<i>&ldquo;It was a nice change that the instructor had personal experience that I could relate to.&rdquo;</i>
								<br />
								<strong>- Medicure Inc.</strong>
							</td>
						</tr>
					</table>
					<br />
					<table class="pharmeng_about2" style="width:100%; padding:5mm;">
						<tr>
							<td style="width:25%;">
								&nbsp;
							</td>
							<td style="width:50%;">
								<i>&ldquo;...good course, especially the case studies.&rdquo;</i>
								<br />
								<strong>- Genesys Venture Inc.</strong>
							</td>
							<td style="width:25%;">
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
				<td style="width:5%; vertical-align:bottom;">
					<font size="48px">&rdquo;</font>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="nusage_footer_container">
		<img alt="NUSAGE Footer" src="{{asset('assets/images/NUSAGEAboutTrainingProviderFooter.jpg')}}" property="image">
		<div class="nusage_footer-logo">
			<img src="{{asset('assets/images/NUS-NUSAGE_logo landscape.png')}}" style="height:100%;">
			&nbsp;&nbsp;
			<img src="{{asset('assets/images/PharmEng_logo landscape.png')}}" style="height:100%; padding: 1mm;">
		</div>
		<div class="nusage_footer-banner">www.PharmEng.asia | Copyright &copy; {{ now()->year }} PharmEng Technology</div>
	</div>
</section>

{{-- PAGE 3 of About the Training Provider --}}
<section id="about_training_provider_3" class="nusage_container">
	<div class="nusage_header_container">
		<img alt="NUS Science Landscape" src="{{asset('assets/images/NUSAGEAboutTrainingProviderHeader.jpg')}}" property="image">
		<div class="nusage_header-banner">About the Training Provider</div>
	</div>
	<br />

	<div class="pharmeng_about">
		<h2 class="pharmeng_about2">
			PharmEng Core Training Courses
		</h2>

		<table width=100% bg-color="#ffffff">
			<tr>
				<td style="width:33%; vertical-align:top; padding:5px;">
					<h3 class="pharmeng_about3">Current Good Manufacturing Practices</h3>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">GMP - Get More Productivity</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">GMP - Concepts and Implementation</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">cGMP's for Drugs and Active Pharmaceutical Ingredients Manufacturers</td>
						</tr>
					</table>
					<br />

					<p class="pharmeng_about3">cGMP training is tailored to meet your company's specific needs in one or all of the following areas:</p>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Engineering</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Production</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Packaging</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Quality Assurance</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Quality Control</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Regulatory Affairs</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Clinical Research</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">New Drug Submission/Application</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Natural Health Products</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Active Pharmaceutical Ingredients</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Medical Devices</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Blood and Blood Products</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Practical cGMP</td>
						</tr>
					</table>
					<br />
		
					<h3 class="pharmeng_about3">Regulatory Affairs</h3>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Good Clinical Practices (GCP)</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">New Drug Submission/Application</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Chemistry, Manufacturing and Control</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Active Pharmaceutical Ingredients</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Natural Health Products Registration</td>
						</tr>
					</table>
							
				</td>
				<td style="width:33%; vertical-align:top; padding:5px;">
					<h3 class="pharmeng_about3">Quality and Compliance</h3>
					<p class="pharmeng_about3">PharmEng also provides customized Good Laboratory Practices (GLP) and Good Clinical Practices (GCP) training to clients in order to assist companies in moving forward with their pre-clinical and clinical trials.</p>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Master Plan - Roadmap to Compliance</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Good Laboratory Practices (GLP)</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Pharmaceutical Quality Assurance and Control</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">GMP Programs - Planning and Implementation</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Audit Programs and Annual Review</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Recall and Compliant Systems</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Standard Operating Procedures</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Corrective and Preventative Actions (CAPA)</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Risk-based Approach to Inspecting Quality Systems</td>
						</tr>
					</table>
					<br />
		
					<h3 class="pharmeng_about3">Validation</h3>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Analytical Methods Validation</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Process Validation</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Cleaning Validation</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Computer Systems Validation</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Validation of Sterilization Processes</td>
						</tr>
					</table>
					<br />
		
					<h3 class="pharmeng_about3">Project Management</h3>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Project Management in a Regulatory Environment</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Project Management for Clinical Research Studies</td>
						</tr>
					</table>
							
				</td>
				<td style="width:33%; vertical-align:top; padding:5px;">
					<h3 class="pharmeng_about3">Medical Devices</h3>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Medical Device Regulatory Requirements</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Quality System Requirements - ISO 13485</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Quality Systems for Medical Devices</td>
						</tr>
					</table>
					<br />
		
					<h3 class="pharmeng_about3">Manufacturing</h3>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Manufacturing Control in the Pharmaceutical Related Industries</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Pharmaceutical and Biotech Manufacturing Processes</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Active Pharmaceutical Manufacturing</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Solid and Semi-Solid Dosage Manufacturing</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Aseptic Manufacturing</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Sterile and Septic Processes</td>
						</tr>
					</table>
					<br />
		
					<h3 class="pharmeng_about3">Engineering</h3>
					<table width=100%  class="pharmeng_about3">
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Commissioning and Validation of Pharmaceutical and Biotechnology Facilities</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Design and Validation of Critical Utility Systems</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Process Analytical Technology (PAT)</td>
						</tr>
						<tr>
							<td style="width:5mm; vertical-align:top;">■</td>
							<td style="vertical-align:top;">Design and Commissioning and Validation of Pharmaceutical and Biotechnology Facilities</td>
						</tr>
					</table>
							
				</td>
				<td style="width:1%; vertical-align:top;">
					&nbsp;
				</td>
			</tr>
		</table>

		<table width=100%>
			<tr>
				<td>
					<h3 class="pharmeng_about3">PharmEng Technology, a division of PE Pharma Inc., headquartered in Toronto, Canada, is a full-service 
						consulting company that serves the pharmaceutical and biotechnology industries internationally. Consulting services include 
						project management, engineering, cGMP, validation, calibration, regulatory compliance and certified training.
					</h3>
				</td>
			</tr>
		</table>

	</div>

	<div class="nusage_footer_container">
		<img alt="NUSAGE Footer" src="{{asset('assets/images/NUSAGEAboutTrainingProviderFooter.jpg')}}" property="image">
		<div class="nusage_footer-logo">
			<img src="{{asset('assets/images/NUS-NUSAGE_logo landscape.png')}}" style="height:100%;">
			&nbsp;&nbsp;
			<img src="{{asset('assets/images/PharmEng_logo landscape.png')}}" style="height:100%; padding: 1mm;">
		</div>
		<div class="nusage_footer-banner">www.PharmEng.asia | Copyright &copy; {{ now()->year }} PharmEng Technology</div>
	</div>
</section>
