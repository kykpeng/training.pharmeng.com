{{--Created by Kenny 2020-04-22. This is specifically for PharmEng Learning Seminar--}}

<h3>No Payment Required</h3>
This is a free training seminar.  Please click "Complete Order" below to confirm your registration.


{!! Form::open(['url' => route('postCreateOrder', ['event_id' => $event->id]), 'class' => 'ajax']) !!}
<div class="offline_payment">
    <h5>If you have special requests (e.g. will join late), please enter below:</h5>
    <div class="well">
        {!! Markdown::parse($event->offline_payment_instructions) !!}
    </div>
    <input class="btn btn-lg btn-success card-submit" style="width:100%;" type="submit" value="Complete Order">
</div>
    {!! Form::close() !!}
<style>
    .offline_payment_toggle {
        padding: 20px 0;
    }
</style>
