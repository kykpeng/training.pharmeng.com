@extends('Public.ViewEvent.Layouts.EventPage')

@section('head')
    @include('Public.ViewEvent.Partials.GoogleTagManager')
@endsection

@section('content')
    @if ($event->organiser->name == 'NUSAGE')

        @include('Public.ViewEvent.Partials.EventNUSAGE')

    @elseif (strpos($event->organiser->name, 'PharmEng') !== false)

        @include('Public.ViewEvent.Partials.EventPharmEngLearninar')

    @else

        @include('Public.ViewEvent.Partials.EventHeaderSection')
        @include('Public.ViewEvent.Partials.EventObjectiveSection')
        @include('Public.ViewEvent.Partials.EventDescriptionSection')
        @include('Public.ViewEvent.Partials.EventOutlineSection')
        @include('Public.ViewEvent.Partials.EventOutcomeSection')
        @include('Public.ViewEvent.Partials.EventAboutInstructorSection')
        @include('Public.ViewEvent.Partials.EventMapSection')
        @include('Public.ViewEvent.Partials.EventOrganiserSection')
        @include('Public.ViewEvent.Partials.EventShareSection')
        @include('Public.ViewEvent.Partials.EventTicketsSection')
        @include('Public.ViewEvent.Partials.EventFooterSection')

    @endif
@stop

