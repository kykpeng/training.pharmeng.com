{{-- PharmEng-specific stylesheet --}}
{!!HTML::style(config('attendize.cdn_url_static_assets').'/assets/stylesheet/learninar.css')!!}

<?php
	// PHP randomizing cover page background image
	$bg = array('NUSAGEbg1.jpg', 'NUSAGEbg2.jpg', 'NUSAGEbg3.jpg' ); // array of filenames

	$i = rand(0, count($bg)-1); // generate random number size of the array
	$selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen
?>

<div style="background-image: url('{{asset('assets/images/'.$selectedBg)}}'); width: 100%; height: 100%; background-size: cover; background-attachment: fixed; background-position: center center;">

{{-- Header Section --}}
<section id="intro" class="learninar_title-container">
    <div class="row">
        <div class="col-md-12">
            <div class="organiser_logo">
                <div class="thumbnail">
                    <img src="{{URL::to($organiser->full_logo_path)}}" />
                </div>
            </div>
            <h1>{{$organiser->name}}</h1>
            @if($organiser->about)
            <div class="description pa10">
                {!! $organiser->about !!}
            </div>
            @endif
        </div>
    </div>
</section>

{{-- Event Section --}}
<section id="events" class="learninar_container">
    <p>All dates shown in Singapore Time.  Please click in to see your local time</p><div class="row">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            @include('Public.ViewOrganiser.Partials.EventListingPanel',
                [
                    'panel_title' => trans("Public_ViewOrganiser.upcoming_events"),
                    'events'      => $upcoming_events
                ]
            )
            @include('Public.ViewOrganiser.Partials.EventListingPanel',
                [
                    'panel_title' => trans("Public_ViewOrganiser.past_events"),
                    'events'      => $past_events
                ]
            )
        </div>
        <div class="col-xs-12 col-md-4">
            @if ($organiser->facebook)
                @include('Shared.Partials.FacebookTimelinePanel',
                    [
                        'facebook_account' => $organiser->facebook
                    ]
                )
            @endif
            @if ($organiser->twitter)
                @include('Shared.Partials.TwitterTimelinePanel',
                    [
                        'twitter_account' => $organiser->twitter
                    ]
                )
            @endif
        </div>
    </div>
</section>

</div>

{{-- Footer Section --}}
<footer id="footer" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                {{--Attendize is provided free of charge on the condition the below hyperlink is left in place.--}}
                {{--See https://github.com/Attendize/Attendize/blob/master/LICENSE for more information.--}}
                @include('Shared.Partials.PoweredBy')

                @if(Utils::userOwns($organiser))
                    &bull;
                    <a class="adminLink"
                       href="{{route('showOrganiserDashboard' , ['organiser_id' => $organiser->id])}}">@lang("Public_ViewOrganiser.organiser_dashboard")</a>
                @endif
            </div>
        </div>
    </div>
</footer>

