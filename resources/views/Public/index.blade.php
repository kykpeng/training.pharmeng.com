<!DOCTYPE html>
<html>
    <head>
        <title>{{ config('attendize.app_name') }} | Pharma Training for Pharma Professionals</title>
        
        <!--Meta-->
        <meta name="description" content="
                                            #Training.PharmEng | Courses, trainings, webinars for for professionals in the pharmaceutical 
                                            industry, and other FDA-regulated industries such as medical device, health supplement, and cosmetics.
                                            Courses in GDP, GMP, GLP, GCP, ICH, PIC/S, ISO 13485, and other GxP regulations, including
                                            regulatory compliance, quality assurance, regulatory affairs, engineering, validation, quality
                                            control, supply chain, process">
        <meta name="keywords" content="Pharma Courses, GMP Training, Good Distribution Practice Training, GLP Training, Clinical Trial GCP,
                                        ICH GCP Training, PIC/S, ISO 13485, Training, Life Science, Pharmaceutical, Medical Device,
                                        Health Supplement, Natural Health Product, Cosmetics">
        <meta name="author" content="Kenny Peng">

        @include('Shared.Partials.GlobalMeta')
        <link rel="canonical" href="{{ env('APP_URL') }}">
        <!--/Meta-->

        <!-- Open Graph data -->
        <meta property="og:title" content="#Training.PharmEng | Pharma Training for Pharma Professionals" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="{{ env('APP_URL') }}" />
        <meta property="og:description" content="
                                            #Training.PharmEng | Courses, trainings, webinars for for professionals in the pharmaceutical 
                                            industry, and other FDA-regulated industries such as medical device, health supplement, and cosmetics.
                                            Courses in GDP, GMP, GLP, GCP, ICH, PIC/S, ISO 13485, and other GxP regulations, including
                                            regulatory compliance, quality assurance, regulatory affairs, engineering, validation, quality
                                            control, supply chain, process" />
        <meta property="og:site_name" content="{{ config('attendize.app_name') }}" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!--CSS-->

        {!! HTML::style(config('attendize.cdn_url_static_assets').'/assets/stylesheet/landing.css') !!}
        {{-- <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/hero-slider.css">
        <link rel="stylesheet" href="css/owl-carousel.css">
        <link rel="stylesheet" href="css/datepicker.css">
        <link rel="stylesheet" href="css/templatemo-style.css">
        --}}
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!--/CSS-->

        <!--JS-->
        {!! HTML::script(config('attendize.cdn_url_static_assets').'/assets/javascript/landing.js') !!}
        {{--<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>--}}
        <!--/JS-->
    </head>

<body>
    <!--Navbar -->
    <!-- Static navbar -->
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#top">#Training.PharmEng</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#events-section">Upcoming Events</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Visit PharmEng <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">Sites</li>
                            <li><a href="https://www.pharmeng.com" target="_blank">PharmEng Global</a></li>
                            <li><a href="https://www.pharmeng.asia" target="_blank">PharmEng Regulatory Affairs</a></li>
                            <li><a href="https://www.pharmeng.tw" target="_blank">PharmEng Engineering Taiwan</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header">Apps</li>
                            <li><a href="https://training.pharmeng.com" target="_blank">#Training.PharmEng</a></li>
                            <li><a href="https://career.pharmeng.asia/careers" target="_blank">#Career.PharmEng</a></li>
                        </ul>
                    </li>
                    <li><a href="#contact-section">Contact Us</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right login-button">
                    <li><a href="/admin">Instructor Login</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!--/.Navbar -->

    @if(session('success'))
        <div class="alert alert-success">
            {!! session('success') !!}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {!! session('error') !!}
        </div>
    @endif
    
    <section class="banner" id="top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="left-banner-content">
                        <div class="text-content">
                            <h6>From Fundamentals to Industry Hot Topics, stay ahead in your industry</h6>
                            <div class="line-dec"></div>
                            <h1>Pharma Courses & Webinars</h1>
                            <div class="white-border-button">
                                <a href="#events-section" class="scroll-link" data-id="best-offer-section">Discover More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="right-banner-content">
                        <div class="logo"><a href="index.html"><img src="assets/images/PharmEng_logo landscape.png" style="max-height: 50px;" alt="PharmEng Logo"></a></div>
                        <h2>PE Training Portal</h2>
                        <span>#Training.PharmEng</span>
                        <div class="line-dec"></div>
                        <p>The leading source of pharma courses and webinars in quality assurance, regulatory affairs, compliance, toxicology, validation, process development,
                            engineering, manufacturing, commercialization in the highly-regulated life science industry including pharmaceuticals, medical devices, cosmetics, and more.</p>
                        <ul class="social-icons">
                            <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/pharmeng-technology/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="https://msng.link/o/?http%3A%2F%2Fweixin.qq.com%2Fr%2FRRxpcdLE4I_frZYS90lX=wc" target="_blank"><i class="fab fa-weixin"></i></a></li>
                            <li><a href="https://line.me/R/ti/p/@@508zuuys" target="_blank"><i class="fab fa-line"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="best-offer" id=best-offer-section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="best-offer-left-content">
                        <div class="icon"><img src="/assets/images/best-offer-icon.png" alt=""></div>
                        <h4>Enquiries Welcome!</h4>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="best-offer-right-content">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h2>Customized Courses</h2>
                                <p>Available upon request.  Contact us if you are looking for <em>customized courses</em> such as
                                    in-house corporate trainings,
                                    public event lectures,
                                    industry-academia programs,
                                    and hosted webinars, with topics in ...</p>
                                <ul>
                                    <li>+ GxP Quality Systems</li>
                                    <li>+ Quality Assurance</li>
                                    <li>+ Regulatory Affairs</li>
                                    <li>+ Compliance and Audits</li>
                                    <li>+ Engineering</li>
                                    <li>+ Process Development</li>
                                    <li>+ Commercialization and Market Expansion</li>
                                </ul>
                                <div class="pink-button">
                                    <a href="#contact-section" class="scroll-link" data-id="events-section">Discover More</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <img src="assets/images/lecture-hall.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="events" id="events-section">
        <div class="content-wrapper">
            <div class="inner-container container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section-heading">
                            <div class="filter-categories">
                                <ul class="project-filter">
                                    <li class="filter" data-filter=".upcoming-events"><span>Upcoming Events</span></li>
                                    <li class="filter" data-filter=".past-events"><span>Past Events</span></li>
                                    <li class="filter" data-filter="all"><span>Show All</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <h2><script>
                        document.write(/\((.*)\)/.exec(new Date().toString())[1]);
                    </script></h2>
                    <div class="col-md-10 col-sm-12 col-md-offset-1">
                        <div class="projects-holder">
                            <div class="event-list">
                                <ul>
                                    {{-- @if(count($upcoming_events)) --}}
                                    @if(count($upcoming_events->where('is_live', 1)))
                                        {{-- @foreach($upcoming_events as $counter=>$event) --}}
                                        @foreach($upcoming_events->where('is_live', 1) as $counter=>$event)
                                            <li class="project-item {{ $counter %2 == 0 ? "even-child" : "odd-child" }} mix upcoming-events">
                                                <ul class="event-item upcoming-events">
                                                    <li>
                                                        <div class="date">
                                                            <span>
                                                                <script>document.write(convertEventDateTime('{!! $event->start_date !!}','short'));</script>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <h4><a href="{{$event->event_url }}">{{ $event->title }}</a></h4>
                                                        <div class="web"">
                                                            <span>Webinar</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="time">
                                                            <span>
                                                                <script>document.write(convertEventDateTime('{!! $event->start_date !!}'));</script>
                                                                <br/>~<br/>
                                                                <script>document.write(convertEventDateTime('{!! $event->end_date !!}'));</script>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="white-button">
                                                            <a href="{{$event->event_url }}">I am interested</a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="project-item even-child mix upcoming-events">
                                            <ul class="event-item upcoming-events">
                                                <li>
                                                    <div class="date">
                                                        <span>--<br>--</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <h4>No Upcoming Events</h4>
                                                    <div class="web">
                                                        <span>--</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="time">
                                                        <span>00:00 --<br>--</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="grey-button">
                                                        <a href="#">--d</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                    
                                    {{-- @if(count($past_events)) --}}
                                    @if(count($past_events->where('is_live', 1)))
                                        {{-- @foreach($past_events as $counter=>$event) --}}
                                        @foreach($past_events->where('is_live', 1) as $counter=>$event)
                                            <li class="project-item past-child mix past-events">
                                                <ul class="event-item past-events">
                                                    <li>
                                                        <div class="date">
                                                            <span>
                                                                <script>document.write(convertEventDateTime('{!! $event->start_date !!}','short'));</script>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <h4><a href="{{$event->event_url }}">{{ $event->title }}</a></h4>
                                                        <div class="web">
                                                            <span>Webinar</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="time">
                                                            <span>
                                                                <script>document.write(convertEventDateTime('{!! $event->start_date !!}'));</script>
                                                                <br/>~<br/>
                                                                <script>document.write(convertEventDateTime('{!! $event->end_date !!}'));</script>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="grey-button">
                                                            <a href="#">Closed</a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="project-item past-child mix past-events">
                                            <ul class="event-item past-events">
                                                <li>
                                                    <div class="date">
                                                        <span>--<br>--</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <h4>No Upcoming Events</h4>
                                                    <div class="web">
                                                        <span>--</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="time">
                                                        <span>00:00 --<br>--</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="grey-button">
                                                        <a href="#">--d</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section>

    {{--
    <section class="testimonial" id="testimonial-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="testimonial-image">
                        <div class="text-content">
                            <h2>Testimonials</h2>
                            <h4>Hear what our customers are saying</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id="owl-testimonial" class="owl-carousel owl-theme">
                        <div class="item col-md-12">
                            <img src="img/author_01.png" alt="Steven Walker">
                            <span>Web Designer</span>
                            <h4>Steven Walker</h4>
                            <br>
                            <p><em>"</em>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cura bitur et sem blandit, rhoncus ante, varius libero. Cras elemen tum tincidunt ullamcorper sed vehic ula dictum.<em>"</em></p>
                        </div>
                        <div class="item col-md-12">
                            <img src="img/author_02.png" alt="Johnny Smith">
                            <span>Web Developer</span>
                            <h4>Johnny Smith</h4>
                            <br>
                            <p><em>"</em>Morbi elit est, pharetra ac enim a, faucibus dignissim augue. Quisque erat erat, placerat non pulvinar eget, sodales eget ex. Cras pulvinar purus et rutrum faucibus.<em>"</em></p>
                        </div>
                        <div class="item col-md-12">
                            <img src="img/author_03.png" alt="William Smoker">
                            <span>Managing Director</span>
                            <h4>William Smoker</h4>
                            <br>
                            <p><em>"</em>Praesent luctus lacinia erat, quis lacinia ipsum varius a. Nullam a velit mollis, suscipit felis vitae, dictum libero hendrerit nibh quis sodales gravida ornare ultricies viverra.<em>"</em></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    --}}    
    
    <section class="services" id="services-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper first-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="/assets/images/consulting.png" alt="Consulting">
                                </div>
                                <h4>Consulting</h4>
                            </div>
                            <div class="back">
                                <p>
                                    Consulting services in quality assurance, regulatory affairs, compliance, validation, process development,
                                    engineering, manufacturing, commercialization in pharmaceuticals, medical devices, cosmetics, neutraceuticals, and more.
                                </p>
                                <span><a href="www.pharmeng.com"><img src="assets/images/PharmEng_logo landscape white.png" style="max-height: 35px;" alt="PharmEng Logo"></a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper second-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="/assets/images/outsourcing.png" alt="Outsourcing">
                                </div>
                                <h4>Outsourcing</h4>
                            </div>
                            <div class="back">
                                <p>
                                    Resource augmentation, offshore technical writing, offshore dossier publishing, remote auditing, pharmacovigilance safety
                                    reporting, compliance filing, marketing authorisation holding, in-country representation, and more. 
                                </p>
                                <span><a href="www.pharmeng.com"><img src="assets/images/PharmEng_logo landscape white.png" style="max-height: 35px;" alt="PharmEng Logo"></a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper third-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="/assets/images/projects.png" alt="Projects">
                                </div>
                                <h4>Projects</h4>
                            </div>
                            <div class="back">
                                <p>
                                    Project Management Office (PMO), turnkey projects, offshoring, engineering projects, validation projects, third-party audit
                                    projects, regulatory affairs projects, go-to-market projects, and more.
                                </p>
                                <span><a href="www.pharmeng.com"><img src="assets/images/PharmEng_logo landscape white.png" style="max-height: 35px;" alt="PharmEng Logo"></a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper fourth-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="/assets/images/training.png" alt="Training">
                                </div>
                                <h4>Training</h4>
                            </div>
                            <div class="back">
                                <p>
                                    Professional trainings and webinars in quality assurance, regulatory affairs, compliance, toxicology, validation, process development, engineering,
                                    manufacturing, commercialization in the highly-regulated life science industry including pharmaceuticals, medical devices, cosmetics, and more.
                                </p>
                                <span><a href="www.pharmeng.com"><img src="assets/images/PharmEng_logo landscape white.png" style="max-height: 35px;" alt="PharmEng Logo"></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contact-us" id="contact-section"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                
                    {!! Form::open(['route' => 'contactUsPost', 'id'=>'contact', 'method'=>'POST']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                <fieldset>
                                    {!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Your Name ...', 'required'=>'required']) !!}
                                </fieldset>
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    {!! Form::email('email', old('email'), ['class'=>'form-control', 'placeholder'=>'Your Email ...', 'required'=>'required']) !!}
                                </fieldset>
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <fieldset>
                                    {!! Form::select('subject', [
                                        'I want to enquire about a course'=>'I want to enquire about a course',
                                        'I want to teach'=>'I want to teach',
                                        'I have other comments or questions'=>'I have other comments or questions',
                                    ],
                                    Input::old('subject'),
                                    ['class'=>'form-control', 'required'=>'required'])
                                    !!}
                                </fieldset>
                                <span class="text-danger">{{ $errors->first('subject') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    {!!  Form::textarea('message', old('message'), ['class'=>'form-control', 'rows'=>6, 'placeholder'=>'Your message...', 'required'=>'required'])  !!}
                                </fieldset>
                                <span class="text-danger">{{ $errors->first('message') }}</span>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <button type="submit" id="form-submit" class="btn">Send it now</button>
                                </fieldset>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-4">
                    <div class="contact-right-content">
                        <a href="https://goo.gl/maps/RiUwzkLns1BwZoGK8">
                            <h4>Find us on maps</h4>
                            <div class="icon"><img src="/assets/images/map-marker-icon.png" alt=""></div>
                        </A>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <p>Copyright &copy; {{ now()->year }} PharmEng Technology 
                </div>
            </div>
        </div>
    </footer>

    {{--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        --}}
    {{--<script src="js/vendor/bootstrap.min.js"></script>--}}
    {{--<script src="js/datepicker.js"></script>--}}
    {!! HTML::script(config('attendize.cdn_url_static_assets').'/assets/javascript/landing-defer.js') !!}
    {{--
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>--}}

    <script type="text/javascript">
    $(document).ready(function() 
	{
        // navigation click actions 
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');         
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 0;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
    </script>

    @include('Shared.Partials.GlobalFooterPublicJS')
</body>
</html>