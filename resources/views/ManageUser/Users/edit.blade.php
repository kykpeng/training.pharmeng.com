@extends('Shared.Layouts.Master')

@section('title')
    @parent
    @lang("Organiser.organiser_events")
@stop

@section('page_title')
    @lang("Organiser.organiser_name_events", ["name"=>$organiser->name])
@stop

@section('top_nav')
    @include('ManageOrganiser.Partials.TopNav')
@stop

@section('head')
    <style>
        .page-header {
            display: none;
        }
    </style>
    <script>
        $(function () {
            $('.colorpicker').minicolors({
                changeDelay: 500,
                change: function () {
                    var replaced = replaceUrlParam('{{route('showOrganiserHome', ['organiser_id'=>$organiser->id])}}', 'preview_styles', encodeURIComponent($('#OrganiserPageDesign form').serialize()));
                    document.getElementById('previewIframe').src = replaced;
                }
            });

        });

        @include('ManageOrganiser.Partials.OrganiserCreateAndEditJS')
    </script>
@stop

@section('menu')
    @include('ManageOrganiser.Partials.Sidebar')
@stop

@section('page_header')

@stop

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2> @lang("User.edit_user")</h2>

        </div>

    </div>

</div>

@if (count($errors) > 0)

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

        @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

        @endforeach

        </ul>

    </div>

@endif

<form method="post" action="{{ URL::to('user/'.$user->id.'/update') }}" accept-charset="UTF-8">

    <input name="_token" type="hidden" value="{{ csrf_token() }}">

<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
            {!! Form::label('first_name', trans("User.first_name"), array('class'=>'control-label required')) !!}
            {!!  Form::text('first_name', $user->first_name, array('class'=>'form-control'))  !!}
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
            {!! Form::label('last_name', trans("User.last_name"), array('class'=>'control-label required')) !!}
            {!!  Form::text('last_name', $user->last_name, array('class'=>'form-control'))  !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
            {!! Form::label('phone', trans("User.phone"), array('class'=>'control-label')) !!}
            {!!  Form::text('phone', $user->phone, array('class'=>'form-control'))  !!}
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
            {!! Form::label('email', trans("User.email"), array('class'=>'control-label required')) !!}
            {!!  Form::text('email', $user->email, array('class'=>'form-control'))  !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="form-group">

            {!! Form::label('Status', trans("User.status"), array('class'=>'control-label')) !!}
            <br/>

            <label>
            {{ Form::checkbox('is_registered', 1, $user->is_registered, array('class' => 'status')) }} @lang("User.is_registered")
            </label>
            <br/>

            <label>
                    {{ Form::checkbox('is_confirmed', 1, $user->is_confirmed, array('class' => 'status')) }} @lang("User.is_confirmed")
            </label> 
            <br/>

            <label>
                    {{ Form::checkbox('is_parent', 1, $user->is_parent, array('class' => 'status')) }} @lang("User.is_parent")
            </label>
            <br/>


        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('zoom_email_id', trans("User.connected_accounts"), array('class'=>'control-label')) !!}
            {!! Form::select('zoom_email_id', $zoom_email_id, $user->zoom_email_id, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="form-group">

            {!! Form::label('Organisers', trans("User.programs"), array('class'=>'control-label ')) !!}
            {!! Form::select('organisers[]', $organisers->pluck('name','id'), $user->organisers()->pluck('id','id')->all(), ['class' => 'form-control', 'id' => 'organisers', 'multiple' => 'multiple']) !!}

            {{-- @foreach($organisers as $value)
                <br/>

                <label>{{ Form::checkbox('organisers[]', $value->id, in_array($value->id, $programs) ? true : false, array('class' => 'name')) }}

                {{ $value->name }}</label>

            @endforeach --}}

        </div>

    </div>
</div>

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="form-group">

            {!! Form::label('Roles', trans("User.roles"), array('class'=>'control-label ')) !!}
            {!! Form::select('roles[]', $roles, $user->roles()->pluck('id','id')->all(), ['class' => 'form-control', 'id' => 'organisers', 'multiple' => 'multiple']) !!}

        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">

        <button type="submit" class="btn btn-primary">@lang("basic.update")</button>
        <a class="btn btn-danger" href="{{route('showAccountSettingsPage', array('organiser_id' => $user->organiser_id))}}">
            @lang("basic.delete")
        </a>
        <a class="btn btn-danger" href="{{route('showAccountSettingsPage', array('organiser_id' => $user->organiser_id))}}">
            @lang("basic.cancel")
        </a>
    </div>

</div>

</form>

@endsection