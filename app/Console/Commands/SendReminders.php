<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SendMessageToAttendees;
use App\Models\Event;
use App\Models\Message;
use App\Models\Organiser;
use Carbon\Carbon;

class SendReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendize:send-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date('r') . ' Running SendReminders...');

        $this->sendReminderEmails();
    }

    private function sendReminderEmails()
    {
        // FIX-ME: See NINJA accountRepo->findWithReminders, add enable_reminder fields 
        $organisers = Organiser::all();
        $now = new Carbon();

        foreach ($organisers as $organiser) {
            // < 1 Hour to the course
            // $events = Event::where('organiser_id','=', $organiser->id)
            //                ->whereBetween('start_date', [$now->copy()->addMinutes(15), $now->copy()->addMinutes(75)])
            //                ->where('is_live', '=', '1')
            //                ->get();
            // The following assumes the crontab is triggered hourly at 15 minutes
            $events = Event::where('organiser_id','=', $organiser->id)
                            ->where(function($query) use ($now) {
                                $query->whereBetween('start_date', [$now->copy()->addMinutes(80), $now->copy()->addMinutes(90)])
                                    ->orWhereBetween('start_date', [$now->copy()->addMinutes(50), $now->copy()->addMinutes(60)]);
                            })
                            ->where('is_live', '=', '1')
                            ->where('reminder2', '=', '1')
                            ->get();
                            
            $this->info(date('r ') . $organiser->name . ': ' . $events->count() . ' events within 1 hour found');

            foreach ($events as $event) {
                $message = Message::createNew(1,1);
                $message->message = 'Dear Participants,

                    Your Webinar is starting in <u><b>1 Hour</b></u>!
                    '
                    . str_replace('&#10;',chr(10),$event->location_address_line_1) . '

                    We look forward to seeing you soon!

                    Yours Sincerely
                    '
                    . ((! empty($event->administrator)) ? $event->administrator->first_name : '@') . ' '
                    . ((! empty($event->administrator)) ? $event->administrator->last_name : 'Training.PharmEng') . chr(10)
                    . ((! empty($event->administrator)) ? $event->administrator->email : 'info@pharmeng.com');
                $message->subject = 'Begins in 1 Hour! - ' . $event->title;
                $message->recipients = 'all';
                $message->event_id = $event->id;
                $message->save();
                
                dispatch_now(new SendMessageToAttendees($message));
                \Log::info('Sending 1 Hour reminder for: ' . $event->title);
            }


            // < 1 Day to the course
            $events = Event::where('organiser_id','=', $organiser->id)
                            ->whereBetween('start_date', [$now->copy()->addMinutes(1410), $now->copy()->addMinutes(1470)])
                            ->where('is_live', '=', '1')
                            ->where('reminder1', '=', '1')
                            ->get();

            $this->info(date('r ') . $organiser->name . ': ' . $events->count() . ' events witin 1 day found');

            foreach ($events as $event) {
                $message = Message::createNew(1,1);
                $message->message = 'Dear Participants,

                    Your Webinar is starting in 1 Day!
                    '
                    . str_replace('&#10;',chr(10),$event->location_address_line_1) . '

                    We look forward to seeing you soon!

                    Yours Sincerely
                    '
                    . ((! empty($event->administrator)) ? $event->administrator->first_name : '@') . ' '
                    . ((! empty($event->administrator)) ? $event->administrator->last_name : 'Training.PharmEng') . chr(10)
                    . ((! empty($event->administrator)) ? $event->administrator->email : 'info@pharmeng.com');
                $message->subject = $event->title . ' - Begins in 1 day!';
                $message->recipients = 'all';
                $message->event_id = $event->id;
                $message->save();
                
                dispatch_now(new SendMessageToAttendees($message));
                \Log::info('Sending 1 Day reminder for: ' . $event->title);
            }
        }
    }
}
