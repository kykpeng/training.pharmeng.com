<?php

namespace App\Mailers;

use App\Models\Order;
use App\Services\Order as OrderService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Log;
use Mail;
use Spatie\CalendarLinks\Link;

class OrderMailer
{
    public function sendOrderNotification(Order $order)
    {
        $orderService = new OrderService($order->amount, $order->organiser_booking_fee, $order->event);
        $orderService->calculateFinalCosts();

        $data = [
            'order' => $order,
            'orderService' => $orderService
        ];

        Mail::send('Emails.OrderNotification', $data, function ($message) use ($order) {
            $message->to($order->account->email);
            $message->subject(trans("Controllers.new_order_received", ["event"=> $order->event->title, "order" => $order->order_reference]));

            /*
            // Modified by Kenny 2020-11-12
            if ((! empty($order->event->instructor_id) && $order->event->bcc_instructor) && (! empty($order->event->administrator_id) && $order->event->bcc_administrator)) {
                $message->bcc(array_unique(array($order->event->instructor->email, $order->event->administrator->email)));
                Log::info("Sending Order Notification to: " . $order->account->email . ", " . $order->event->instructor->email . ", " . $order->event->administrator->email);
            } elseif (! empty($order->event->instructor_id) && $order->event->bcc_instructor) {
                $message->bcc($order->event->instructor->email);
                Log::info("Sending Order Notification to: " . $order->account->email . ", " . $order->event->instructor->email);
            } elseif (! empty($order->event->administrator_id) && $order->event->bcc_administrator) {
                $message->bcc($order->event->administrator->email);
                Log::info("Sending Order Notification to: " . $order->account->email . ", " . $order->event->administrator->email);            }
            // End Modification
            */
        });

    }

    public function sendOrderTickets(Order $order)
    {
        $orderService = new OrderService($order->amount, $order->organiser_booking_fee, $order->event);
        $orderService->calculateFinalCosts();

        Log::info("Sending ticket to: " . $order->email);
        $data = [
            'order' => $order,
            'orderService' => $orderService
        ];

        $file_name = $order->order_reference;
        $file_path = public_path(config('attendize.event_pdf_tickets_path')) . '/' . $file_name . '.pdf';
        if (!file_exists($file_path)) {
            Log::error("Cannot send actual ticket to : " . $order->email . " as ticket file does not exist on disk");
            return;
        }

        // create ics file
        $url = route('downloadCalendarIcs', ['event_id' => $order->event->id]);
        $icsFileName = 'event_'.$order->event->id .'.ics';
        Storage::put($icsFileName, file_get_contents($url));

        Mail::send('Mailers.TicketMailer.SendOrderTickets', $data, function ($message) use ($order, $file_path, $icsFileName) {
            $message->to($order->email);
            $message->subject(trans("Controllers.tickets_for_event", ["event" => $order->event->title]));
            $message->attach($file_path);
            $message->attach(public_path('user_content'.DIRECTORY_SEPARATOR .$icsFileName));
        });

    }

}
