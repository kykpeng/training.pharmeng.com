<?php

namespace App\Http\Controllers;

use App\Attendize\Utils;
use App\Models\Organiser;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NUSAGEController extends Controller
{
    /**
     * Show the default 'Landing' page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // NUSAGE's $organiser_id = 1
        $organiser = Organiser::findOrFail(1);

        if (!$organiser->enable_organiser_page && !Utils::userOwns($organiser)) {
            abort(404);
        }

        $upcoming_events = $organiser->events()->where('end_date', '>=', Carbon::now())->get();
        // $past_events = $organiser->events()->where('end_date', '<', Carbon::now())->limit(10)->get();
        $past_events = $organiser->events()->where('end_date', '<', Carbon::now())->orderBy('end_date',"desc")->take(20)->get();
        // \Log::info(json_encode($past_events));
        $data = [
            'organiser'       => $organiser,
            'tickets'         => $organiser->events()->orderBy('created_at', 'desc')->get(),
            'is_embedded'     => 0,
            'upcoming_events' => $upcoming_events,
            'past_events'     => $past_events,
        ];

        return view('Public.NUSAGE.index', $data);
    }

}
