<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Organiser;
use DB;

class UserController extends Controller
{

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($user_id)
    {
        $user = User::find($user_id);
        $roles = Role::all()->pluck('name','id');
        $organiser = Organiser::scope()->findOrFail($user->organiser_id);
        $organisers = Organiser::all();
        // $programs = Organiser::where('account_id', $user->account_id)
        //             ->pluck('name','id')->all();
        $zoom_email_id = [
            'null' => 'Select Account',
            'pti@pharmeng.com' => 'pti@pharmeng.com',
            'pti.eu@pharmeg.com' => 'pti.eu@pharmeg.com',
            'info.asia@pharmeng.com' => 'info.asia@pharmeng.com'
        ];

        $data = [
            'user' => $user,
            'roles' => $roles,
            'organiser' => $organiser,
            'organisers' => $organisers,
            // 'programs' => $programs,
            'zoom_email_id' => $zoom_email_id,
        ];

        return view('ManageUser.Users.edit', $data);
        
        // $user = User::find($user_id);
        // $roles = Role::pluck('name','name')->all();
        // // $organisers = Organiser::all();
        // // $userRole = $user->roles->pluck('name','name')->all();
        // // return view('users.edit',compact('user','roles','userRole'));

        // $data = [
        //     'user' => $user,
        //     'roles' => $roles,
        //     // 'userRole' => $userRole
        // ];
        // return view('ManageUser.Modals.EditMyUser', $data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }

    /**
     * Show the edit user modal
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function showEditUser()
    {
        $data = [
            'user' => Auth::user(),
        ];

        return view('ManageUser.Modals.EditUser', $data);
    }

    /**
     * Updates the current user
     *
     * @param Request $request
     * @return mixed
     */
    public function postEditUser(Request $request)
    {
        $rules = [
            'email'        => [
                'required',
                'email',
                'unique:users,email,' . Auth::user()->id . ',id,account_id,' . Auth::user()->account_id
            ],
            'password'     => 'passcheck',
            'new_password' => ['min:8', 'confirmed', 'required_with:password'],
            'first_name'   => ['required'],
            'last_name'    => ['required'],
        ];

        $messages = [
            'email.email'         => trans("Controllers.error.email.email"),
            'email.required'      => trans("Controllers.error.email.required"),
            'password.passcheck'  => trans("Controllers.error.password.passcheck"),
            'email.unique'        => trans("Controllers.error.email.unique"),
            'first_name.required' => trans("Controllers.error.first_name.required"),
            'last_name.required'  => trans("Controllers.error.last_name.required"),
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validation->messages()->toArray(),
            ]);
        }

        $user = Auth::user();

        if ($request->get('password')) {
            $user->password = Hash::make($request->get('new_password'));
        }

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');

        $user->save();

        return response()->json([
            'status'  => 'success',
            'message' => trans("Controllers.successfully_saved_details"),
        ]);
    }

    public function postEditOtherUser(Request $request, $id)
    {
        $rules = [
            'email' => 'required|email|unique:users,email,'.$id,
            'password'     => 'passcheck',
            'new_password' => ['min:8', 'confirmed', 'required_with:password'],
            'first_name'   => ['required'],
            'last_name'    => ['required'],
            'phone'        => ['min:10', 'max:10'],
        ];

        $messages = [
            'email.email'         => trans("Controllers.error.email.email"),
            'email.required'      => trans("Controllers.error.email.required"),
            'password.passcheck'  => trans("Controllers.error.password.passcheck"),
            'email.unique'        => trans("Controllers.error.email.unique"),
            'first_name.required' => trans("Controllers.error.first_name.required"),
            'last_name.required'  => trans("Controllers.error.last_name.required"),
            'phone.min'           => trans("Controllers.error.phone.min"),
            'phone.max'           => trans("Controllers.error.phone.max"),
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validation->messages()->toArray(),
            ]);
        }

        // $user = Auth::user();
        $user = User::find($id);

        if ($request->get('password')) {
            $user->password = Hash::make($request->get('new_password'));
        }

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->phone = isset($request->phone) ? $request->get('phone') : $user->phone;
        $user->is_registered = isset($request->is_registered) ? $request->get('is_registered') : 0;
        $user->is_confirmed = isset($request->is_confirmed) ? $request->get('is_confirmed') : 0;
        $user->is_parent = isset($request->is_parent) ? $request->get('is_parent') : 0;
        $user->zoom_email_id = isset($request->zoom_email_id) ? $request->get('zoom_email_id') : $user->zoom_email_id;
        $user->organisers()->detach();
        $user->organisers()->attach(isset($request->organisers) ? $request->get('organisers') : []);
        $user->syncRoles(isset($request->roles) ? $request->get('roles') : []);

        $user->save();

        return redirect()->route('showAccountSettingsPage', array('organiser_id' => $user->organiser_id))
                        ->with('success', trans("Controllers.successfully_saved_details"));

        // return response()->json([
        //     'status'  => 'success',
        //     'message' => trans("Controllers.successfully_saved_details"),
        // ]);
    }
}
