<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\ContactUs;
use Mail;

class ContactUsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUsPost(Request $request)
    {
        $this->validate($request, [
         'name' => 'required',
         'email' => 'required|email',
         'subject' => 'required',
         'message' => 'required'
         ]);
        ContactUs::create($request->all());
        
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'message_body' => $request->get('message')
        ];

        try {
            Mail::send('Emails.ContactUsNotification',$data, function ($message) use ($data) {
                $message->to('kenny.p@pharmeng.com');
                $message->subject('Enquiry: ' . $data['subject']);
            });
        } catch(\Exception $e) {
            Log::error("Cannot send Contact Us message.");
            Log::error("Error message. " . $e->getMessage());
            Log::error("Error stack trace" . $e->getTraceAsString());
            $this->fail($e);
            return back()->with('error', 'Temporary server error.  Your message was not sent.');
        }

        return back()->with('success', 'Thank you for contacting us!');
    }
 
}
