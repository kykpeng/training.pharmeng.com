<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Permission;
use DB;
use App\Models\Organiser;
use App\Models\Account;
use Auth;
use App\Models\Timezone;
use App\Models\Currency;
use App\Models\PaymentGateway;
use App\Models\AccountPaymentGateway;



class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       

       // $roles = Role::orderBy('id','DESC')->paginate(5);
       $data = [
        'organiser' => Organiser::scope()->findOrFail(1),
    ];

    $data = [
        'organiser' => Organiser::scope()->findOrFail(1),
        'organisers' => Organiser::all(),
        'account'                  => Account::find(Auth::user()->account_id),
        'timezones'                => Timezone::pluck('location', 'id'),
        'currencies'               => Currency::pluck('title', 'id'),
        'payment_gateways'         => PaymentGateway::getAllWithDefaultSet(),
        'default_payment_gateway_id' => PaymentGateway::getDefaultPaymentGatewayId(),
        'account_payment_gateways' => AccountPaymentGateway::scope()->get(),
        'roles'                    => Role::orderBy('id','DESC')->paginate(5),
    ];
        return view('roles.index',$data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'organiser' => Organiser::scope()->findOrFail(1),
            'organisers' => Organiser::all(),
            'account'                  => Account::find(Auth::user()->account_id),
            'timezones'                => Timezone::pluck('location', 'id'),
            'currencies'               => Currency::pluck('title', 'id'),
            'payment_gateways'         => PaymentGateway::getAllWithDefaultSet(),
            'default_payment_gateway_id' => PaymentGateway::getDefaultPaymentGatewayId(),
            'account_payment_gateways' => AccountPaymentGateway::scope()->get(),
            'permission'                    => Permission::get(),
        ];
        //$permission = Permission::get();
        return view('roles.create',$data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));

        return redirect('roles')
            ->with('success','Role created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();

        return view('roles.show',compact('role','rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = [
            'organiser' => Organiser::scope()->findOrFail(1),
            'organisers' => Organiser::all(),
            'account'                  => Account::find(Auth::user()->account_id),
            'timezones'                => Timezone::pluck('location', 'id'),
            'currencies'               => Currency::pluck('title', 'id'),
            'payment_gateways'         => PaymentGateway::getAllWithDefaultSet(),
            'default_payment_gateway_id' => PaymentGateway::getDefaultPaymentGatewayId(),
            'account_payment_gateways' => AccountPaymentGateway::scope()->get(),
            'role' => Role::find($id),
            'permission'                    => Permission::get(),
            'rolePermissions' => DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all()
        ];
        // $role = Role::find($id);
        // $permission = Permission::get();
        // $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
        //     ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
        //     ->all();
        return view('roles.edit',$data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permission'));

        return redirect('roles')
            ->with('success','Role updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect('roles')
            ->with('success','Role deleted successfully');
    }
}
