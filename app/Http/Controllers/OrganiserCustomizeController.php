<?php

namespace App\Http\Controllers;

use App\Models\Organiser;
use File;
use Image;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\View;

use App\Models\Account;
use App\Models\AccountPaymentGateway;
use App\Models\Currency;
use App\Models\PaymentGateway;
use App\Models\Timezone;
use App\Models\User;
use Auth;
use Hash;
use HttpClient;
use Input;
use Mail;
use Services\PaymentGateway\Dummy;
use Services\PaymentGateway\Stripe;
use Services\PaymentGateway\StripeSCA;
use GuzzleHttp\Client;
use App\Models\Role;
use App\Models\Permission;

class OrganiserCustomizeController extends MyBaseController
{
    public function showAccountSettings($organiser_id) {
        $data = [
            'organiser' => Organiser::scope()->findOrFail($organiser_id),
        ];

        $users = User::select(['id', 'name', 'email', 'phone', 'created_at'])->with('roles');

        $data = [
            'organiser' => Organiser::scope()->findOrFail($organiser_id),
            'account'                  => Account::find(Auth::user()->account_id),
            'timezones'                => Timezone::pluck('location', 'id'),
            'currencies'               => Currency::pluck('title', 'id'),
            'payment_gateways'         => PaymentGateway::getAllWithDefaultSet(),
            'default_payment_gateway_id' => PaymentGateway::getDefaultPaymentGatewayId(),
            'account_payment_gateways' => AccountPaymentGateway::scope()->get(),
            'version_info'             => $this->getVersionInfo(),
            'roles'                    => $roles = Role::all()
        ];

        if (View::exists('ManageOrganiser.ManageAccountSetttings')) {
            echo "view exist";
            return view('ManageOrganiser.ManageAccountSetttings', $data);
        } else {
            echo "no view found";
        }
    }

    public function getVersionInfo()
    {
        $installedVersion = null;
        $latestVersion = null;

        try {
            $http_client = new Client();

            $response = $http_client->get('https://attendize.com/version.php');
            $latestVersion = (string)$response->getBody();
            $installedVersion = file_get_contents(base_path('VERSION'));
        } catch (\Exception $exception) {
            return [
                'latest'      => 0,
                'installed'   => 0,
                'is_outdated' => true,
            ];
        }

        if ($installedVersion && $latestVersion) {
            return [
                'latest'      => $latestVersion,
                'installed'   => $installedVersion,
                'is_outdated' => (version_compare($installedVersion, $latestVersion) === -1) ? true : false,
            ];
        }

        return false;
    }

    /**
     * Show organiser setting page
     *
     * @param $organiser_id
     * @return mixed
     */
    public function showCustomize($organiser_id)
    {
        $data = [
            'organiser' => Organiser::scope()->findOrFail($organiser_id),
        ];

        return view('ManageOrganiser.Customize', $data);
    }

    /**
     * Edits organiser settings / design etc.
     *
     * @param Request $request
     * @param $organiser_id
     * @return mixed
     */
    public function postEditOrganiser(Request $request, $organiser_id)
    {
        $organiser = Organiser::scope()->find($organiser_id);

        $chargeTax = $request->get('charge_tax');
        if ($chargeTax == 1) {
            $organiser->addExtraValidationRules();
        }

        if (!$organiser->validate($request->all())) {
            return response()->json([
                'status'   => 'error',
                'messages' => $organiser->errors(),
            ]);
        }

        $organiser->name = $request->get('name');
        $organiser->about = $request->get('about');
        $organiser->google_analytics_code = $request->get('google_analytics_code');
        $organiser->google_tag_manager_code = $request->get('google_tag_manager_code');
        $organiser->email = $request->get('email');
        $organiser->enable_organiser_page = $request->get('enable_organiser_page');
        $organiser->facebook = $request->get('facebook');
        $organiser->twitter = $request->get('twitter');

        $organiser->tax_name = $request->get('tax_name');
        $organiser->tax_value = $request->get('tax_value');
        $organiser->tax_id = $request->get('tax_id');
        $organiser->charge_tax = ($request->get('charge_tax') == 1) ? 1 : 0;

        if ($request->get('remove_current_image') == '1') {
            $organiser->logo_path = '';
        }

        if ($request->hasFile('organiser_logo')) {
            $organiser->setLogo($request->file('organiser_logo'));
        }

        $organiser->save();

        session()->flash('message', trans("Controllers.successfully_updated_organiser"));

        return response()->json([
            'status'      => 'success',
            'redirectUrl' => '',
        ]);
    }

    /**
     * Edits organiser profile page colors / design
     *
     * @param Request $request
     * @param $organiser_id
     * @return mixed
     */
    public function postEditOrganiserPageDesign(Request $request, $organiser_id)
    {
        $organiser = Organiser::scope()->findOrFail($organiser_id);

        $rules = [
            'page_bg_color'        => ['required'],
            'page_header_bg_color' => ['required'],
            'page_text_color'      => ['required'],
        ];
        $messages = [
            'page_header_bg_color.required' => trans("Controllers.error.page_header_bg_color.required"),
            'page_bg_color.required'        => trans("Controllers.error.page_bg_color.required"),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'messages' => $validator->messages()->toArray(),
            ]);
        }

        $organiser->page_bg_color        = $request->get('page_bg_color');
        $organiser->page_header_bg_color = $request->get('page_header_bg_color');
        $organiser->page_text_color      = $request->get('page_text_color');

        $organiser->save();

        return response()->json([
            'status'  => 'success',
            'message' => trans("Controllers.organiser_design_successfully_updated"),
        ]);
    }
}
